package com.ext.test;


//Consumer 接口接收一个泛型参数，然后调用 accept，对这个参数做一系列消费操作
import java.util.function.Consumer;
public class test3 {

//    public static void main(String[] args) {
//        Consumer<Integer> consumer = x -> {
//            int a = x + 6;
//            System.out.println(a);
//            System.out.println("大彬" + a);
//        };
//        consumer.accept(660);
//
//    }


    public static void main(String[] args) {
        //在stream里，对入参做一些操作，主要是用于forEach，对传入的参数，做一系列的业务操作
        Consumer<Integer> consumer1 = x -> System.out.println("first x : " + x);
        Consumer<Integer> consumer2 = x -> {
            System.out.println("second x : " + x);
            throw new NullPointerException("throw exception second");
        };
        Consumer<Integer> consumer3 = x -> System.out.println("third x : " + x);

        consumer1.andThen(consumer2).andThen(consumer3).accept(1);
    }
}
