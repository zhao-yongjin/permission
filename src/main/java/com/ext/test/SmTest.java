package com.ext.test;

/**
 * md5加密
 */

import cn.hutool.core.codec.Base64;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;
import cn.hutool.crypto.digest.DigestAlgorithm;
import cn.hutool.crypto.digest.DigestUtil;
import cn.hutool.crypto.digest.Digester;
import cn.hutool.crypto.symmetric.AES;
import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import cn.hutool.crypto.symmetric.SymmetricCrypto;
import org.junit.Test;


public class SmTest {
    @Test
    public void test2() {
        String content = "君不见黄SecureUtil河之水天上来，奔流到海不复回";
        String encrypt = SecureUtil.md5(content);

        System.out.println("加密前的内容：" + content);
        System.out.println("加密后的内容：" + encrypt);
    }

    //摘要加密算法
    @Test
    public void fun1() {
        //获取摘要
        //MD5
        String content = "君不见黄SecureUtil河之水天上来，奔流到海不复回";
        String encrypt = SecureUtil.md5(content);

        System.out.println("加密前的内容：" + content);
        System.out.println("加密后的内容：" + encrypt);

        //方式一
        Digester md5 = new Digester(DigestAlgorithm.MD5);
        String str = md5.digestHex(content);
        System.out.println(str);

        //方式二
        String string = DigestUtil.md5Hex(content);
        System.out.println(string);
    }

    //非对称加密
    @Test
    public void fun2() {
        //非对称加密
        RSA rsa = new RSA();
        //获得私钥
        rsa.getPrivateKey();
        rsa.getPrivateKeyBase64();
        //获得公钥
        rsa.getPublicKey();
        rsa.getPublicKeyBase64();

        //公钥加密，私钥加密
        byte[] encrypt = rsa.encrypt(StrUtil.bytes("RAS测试", CharsetUtil.CHARSET_UTF_8), KeyType.PublicKey);
        byte[] decrypt = rsa.decrypt(encrypt, KeyType.PrivateKey);
        String encode = Base64.encode(decrypt);
        System.out.println(encode);
        //私钥加密，公钥解密
        byte[] encrypt2 = rsa.encrypt(StrUtil.bytes("RAS测试", CharsetUtil.CHARSET_UTF_8), KeyType.PrivateKey);
        byte[] decrypt2 = rsa.decrypt(encrypt2, KeyType.PublicKey);
        String encode2 = Base64.encode(decrypt2);
        System.out.println(encode2);
    }

    @Test
    public void fun4() {
        String content = "asdf1234";
        byte[] key = "hahaaha".getBytes();
        //构建
        SymmetricCrypto aes = new SymmetricCrypto(SymmetricAlgorithm.AES, key);
        //加密
        byte[] encrypt = aes.encrypt(content);
        //解密
        byte[] decrypt = aes.decrypt(encrypt);
    }

    //对称加密
    @Test
    public void fun3() {
        //第一种：以AES算法
        String content = "test中文";
        //随机生成密钥
        byte[] key = SecureUtil.generateKey(SymmetricAlgorithm.AES.getValue()).getEncoded();
        //构建
        SymmetricCrypto aes = new SymmetricCrypto(SymmetricAlgorithm.AES, key);
        //加密
        byte[] encrypt = aes.encrypt(content);
        //解密
        byte[] decrypt = aes.decrypt(encrypt);

        //加密16进制表示
        String encryptHex = aes.encryptHex(content);
        System.out.println("AES加密16进制表示：" + encryptHex);
        //解密为字符串
        String decryptStr = aes.decryptStr(encryptHex, CharsetUtil.CHARSET_UTF_8);
        System.out.println("AES解密为字符串：" + decryptStr);

        //第二种 DESede加密
        String content2 = "test中文";
        byte[] key2 = SecureUtil.generateKey(SymmetricAlgorithm.DESede.getValue()).getEncoded();
        SymmetricCrypto des = new SymmetricCrypto(SymmetricAlgorithm.DESede, key2);
        //加密
        byte[] encrypt2 = des.encrypt(content2);
        //解密
        byte[] decrypt2 = des.decrypt(encrypt2);

        //加密为16进制字符串（Hex表示)
        String encryptHex2 = des.encryptHex(content2);
        System.out.println("DESede加密16进制表示：" + encryptHex2);
        String decryptStr2 = des.decryptStr(encryptHex2);
        System.out.println("DESede解密为字符串：" + decryptStr2);

        //第三种AES封装
        String content3 = "test中文";
        //随机生成密钥
        byte[] key3 = SecureUtil.generateKey(SymmetricAlgorithm.AES.getValue()).getEncoded();
        //构建
        AES aes3 = SecureUtil.aes(key3);
        //加密
        byte[] encrypt3 = aes3.encrypt(content3);
        //解密
        byte[] decrypt3 = aes3.decrypt(encrypt3);
        //加密为16进制表示
        String encryptHex3 = aes3.encryptHex(content3);
        System.out.println("AES封装加密16进制表示：" + encryptHex3);
        //解密为字符串
        String decryptStr3 = aes3.decryptStr(encryptHex3, CharsetUtil.CHARSET_UTF_8);
        System.out.println("AES封装解密为字符串：" + decryptStr3);
    }
}


