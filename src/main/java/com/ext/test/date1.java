package com.ext.test;

import com.ext.commons.utils.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class date1 {
    public static void main(String[] args) throws ParseException {
//        Calendar c = Calendar.getInstance();
//        c.set(Calendar.HOUR_OF_DAY, 10);
//        System.out.println(c.getTime());

//        Calendar calendar = Calendar.getInstance();
//        calendar.set(2019, Calendar.DECEMBER, 31);
//        Date testDate = calendar.getTime();
//        SimpleDateFormat dtf = new SimpleDateFormat("yyyy-MM-dd");
//        System.out.println("2019-12-31 转 YYYY-MM-dd 格式后 " + dtf.format(testDate));


//
//        String str = "2020-03-18 12:00";
//        SimpleDateFormat dtf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
//        Date newDate = null;
//        try {
//            newDate = dtf.parse(str);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        System.out.println(newDate);
//
//        System.out.println("当前"+calendar.get(Calendar.MONTH)+"月份");



//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
//        String time = "2020-03";
//        System.out.println(sdf.parse(time));

        //自定义一个字符串
        String str = new String("13,235sdf+fnk,055s,fkd, ");
        //注意这里字符串最后是空，下面会用到
        //这里我们假设使用","作为分隔符
//        String newStr = str.split(",")[0];

//        //想要看到最后的结果可以通过循环进行输出
//        for(int i = 0;i < newStr.length;i++){
//            System.out.println(newStr[i]);
//        }
        //两个分隔符的情况  ","和"+"作为分隔符
        //同样还是返回字符串数组类型的数据
        //这里我实验之后发现给"+"加上"[]"之后才可以通过编译，得到正确的结果
        String[] newStr2 = str.split(",|[+]");

        for(int i = 0;i < newStr2.length;i++) {
            System.out.println(newStr2[i]);
        }

    }

}
