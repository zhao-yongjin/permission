package com.ext.test;

public class Dog extends Animal{
    //重写（父类，子类方法名一致，参数列表一致，访问权限大于父类）
    //返回值和父类类型相同或者是父类类型的子类
    @Override
    public Integer test(int num) {
        return super.test(num);
    }
}
