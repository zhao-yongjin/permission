package com.ext.test;



import org.apache.commons.collections.CollectionUtils;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class list {
    public static void main(String[] args) {


        List<Integer> list1 = Arrays.asList(1,2,3);
        List<Integer> list2 = Arrays.asList(2,4,5);
        //获取并集
        Collection<Integer> unionList = CollectionUtils.union(list1, list2);
        System.out.println(unionList);

        //获取交集
        Collection<Integer> intersectionList = CollectionUtils.intersection(list1, list2);
        System.out.println(intersectionList);

        //获取交集的补集
        Collection<Integer> disjunctionList = CollectionUtils.disjunction(list1, list2);
        System.out.println(disjunctionList);

        //获取差集
        Collection<Integer> subtractList = CollectionUtils.subtract(list1, list2);
        System.out.println(subtractList);


//       List<user> list = Arrays.asList(
//               new user("tom",0,121),
//               new user("wq",32,42),
//               new user("erw",32,121)
//       );
//       list.get(0).setAge(15);
//        HashMap<String,Object> map = new HashMap<>();
//        map.put("用户",list);
//        map.put("顾客","100个");
////        for (user user : list) {
////            user.setAge(82);
////        }
//        System.out.println(map);
    }
}
