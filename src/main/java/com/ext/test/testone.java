package com.ext.test;

import cn.hutool.core.util.NumberUtil;

import java.math.BigDecimal;

public class testone {
    public static void main(String[] args) {
        BigDecimal b1 = new BigDecimal("8.256");
        BigDecimal b2 = new BigDecimal("6.347");

        //加法
        BigDecimal addResult = NumberUtil.add(b1, b2);
        System.out.println("加法运算结果：" + addResult);

        //减法
        BigDecimal subResult = NumberUtil.sub(b1, b2);
        System.out.println("减法运算结果：" + subResult);

        //乘法
        BigDecimal mulResult = NumberUtil.mul(b1, b2);
        System.out.println("乘法运算结果：" + mulResult);

        //除法
        BigDecimal divResult = NumberUtil.div(b1, b2);
        System.out.println("除法运算结果：" + divResult);

        //保留两位小数
        BigDecimal roundResult = NumberUtil.round(b1, 2);
        System.out.println("保留两位小数：" + roundResult);
    }
}
