package com.ext.test;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class test5 {
    public static void main(String[] args) {
//        LocalDateTime now = LocalDateTime.now();   //带时分秒
//        int hour = now.getHour();
//        int minute = now.getMinute();
//        int second = now.getSecond();
//        System.out.println(hour);
//        System.out.println(minute);
//        System.out.println(second);

//        LocalDate now = LocalDate.now();    //不带时分秒
//        int year = now.getYear();
//        int monthValue = now.getMonthValue();
//        int dayOfMonth = now.getDayOfMonth();
//        System.out.println(year);
//        System.out.println(monthValue);
//        System.out.println(dayOfMonth);

//        LocalDate date = LocalDate.of(2018,2,6);    //转换日期格式
//        System.out.println("自定义日期:"+date);

//          LocalDate now = LocalDate.now();
//          LocalDate of = LocalDate.of(2018, 2, 5);  //判断两个日期是否相等
//          if(now.equals(of)){
//              System.out.println("时间相等");
//          }else{
//              System.out.println("时间不等");
//          }

//        LocalTime now = LocalTime.now();   //获取当前时间
//        System.out.println("获取当前的时间,不含有日期:"+now);

//          LocalTime time = LocalTime.now();
//          LocalTime localTime = time.plusHours(3);  //3小时后的时间
//        LocalTime localTime = time.plusMinutes(3);  //3分钟后时间
//        LocalTime localTime = time.plusSeconds(3);   //3秒钟后时间
//        System.out.println(localTime);

//        LocalDate today = LocalDate.now();
//        System.out.println("今天的日期为:"+today);
//        LocalDate nextWeek = today.plus(1, ChronoUnit.WEEKS);   //计算一周后的日期
//        System.out.println("一周后的日期为:"+nextWeek);

//        LocalDate today = LocalDate.now();
//        LocalDate previousYear = today.minus(1, ChronoUnit.YEARS);//一年前的日期
//        System.out.println("一年前的日期 : " + previousYear);
//        LocalDate nextYear = today.plus(1, ChronoUnit.YEARS);        //一年后的日期
//        System.out.println("一年后的日期:" + nextYear);
//
//        Clock clock = Clock.systemUTC();
//        System.out.println("Clock : " + clock.millis());
//
//        Clock defaultClock = Clock.systemDefaultZone();
//        System.out.println("Clock : " + defaultClock.millis());

//        LocalDate today = LocalDate.now();
//        LocalDate tomorrow = LocalDate.of(2022,11,11);
//        if(tomorrow.isAfter(today)){
//            System.out.println("之后的日期:"+tomorrow);
//        }
//        LocalDate yesterday = today.minus(1, ChronoUnit.DAYS);
//        if(yesterday.isBefore(today)){
//            System.out.println("之前的日期:"+yesterday);
//        }

//        ZoneId america = ZoneId.of("America/New_York");
//        LocalDateTime localtDateAndTime = LocalDateTime.now();
//        ZonedDateTime dateAndTimeInNewYork  = ZonedDateTime.of(localtDateAndTime, america );
//        System.out.println("Current date and time in a particular timezone : " + dateAndTimeInNewYork);

//        String dayAfterTommorrow = "20180205";
//        LocalDate formatted = LocalDate.parse(dayAfterTommorrow,DateTimeFormatter.BASIC_ISO_DATE);
//        System.out.println(dayAfterTommorrow+"  格式化后的日期为:  "+formatted);
//
        LocalDateTime date = LocalDateTime.now();
        DateTimeFormatter format1 = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  //日期转换为字符串
        //日期转字符串
        String str = date.format(format1);
        System.out.println("日期转换为字符串:"+str);

        DateTimeFormatter format2 = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        //字符串转日期
        LocalDate date2 = LocalDate.parse(str,format2);
        System.out.println("日期类型:"+date2);






    }
}
