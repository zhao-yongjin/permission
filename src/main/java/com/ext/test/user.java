package com.ext.test;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("t_dept")
public class user {

    @ApiModelProperty(value = "部门名称", required = true)
    private String name;

    @ApiModelProperty("工资")
    private Integer age;

    @ApiModelProperty("工资")
    private Integer salary;
}
