package com.ext.test;

import com.alibaba.excel.annotation.ExcelProperty;
//设置表头和添加的数据字段
public class DemoData {
    //设置表头名称
    @ExcelProperty(value = "序号",index = 0)
    private int number;
    //设置表头名称
    @ExcelProperty(value = "类别",index = 1)
    private String category;
    @ExcelProperty(value = "学生姓名",index = 2)
    private String partsname;
    @ExcelProperty(value = "零部件(材料)名称",index = 3)
    private String specifications;
    @ExcelProperty(value = "数量",index = 4)
    private String count;
    @ExcelProperty(value = "备注",index = 5)
    private String note;


    public DemoData() {
    }

    public DemoData(int number, String category, String partsname, String specifications, String count, String note) {
        this.number = number;
        this.category = category;
        this.partsname = partsname;
        this.specifications = specifications;
        this.count = count;
        this.note = note;
    }

    /**
     * 获取
     * @return number
     */
    public int getNumber() {
        return number;
    }

    /**
     * 设置
     * @param number
     */
    public void setNumber(int number) {
        this.number = number;
    }

    /**
     * 获取
     * @return category
     */
    public String getCategory() {
        return category;
    }

    /**
     * 设置
     * @param category
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * 获取
     * @return partsname
     */
    public String getPartsname() {
        return partsname;
    }

    /**
     * 设置
     * @param partsname
     */
    public void setPartsname(String partsname) {
        this.partsname = partsname;
    }

    /**
     * 获取
     * @return specifications
     */
    public String getSpecifications() {
        return specifications;
    }

    /**
     * 设置
     * @param specifications
     */
    public void setSpecifications(String specifications) {
        this.specifications = specifications;
    }

    /**
     * 获取
     * @return count
     */
    public String getCount() {
        return count;
    }

    /**
     * 设置
     * @param count
     */
    public void setCount(String count) {
        this.count = count;
    }

    /**
     * 获取
     * @return note
     */
    public String getNote() {
        return note;
    }

    /**
     * 设置
     * @param note
     */
    public void setNote(String note) {
        this.note = note;
    }

    public String toString() {
        return "DemoData{number = " + number + ", category = " + category + ", partsname = " + partsname + ", specifications = " + specifications + ", count = " + count + ", note = " + note + "}";
    }
}
