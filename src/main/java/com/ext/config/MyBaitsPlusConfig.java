package com.ext.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

// MP的配置类
@Configuration
@EnableTransactionManagement
@MapperScan("com.ext.mapper")
public class MyBaitsPlusConfig {

    // 设置一个分页的拦截器
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        // 1.设置一个MP拦截器
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        // 2.给拦截器中添加一个分页的的拦截器
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
        return interceptor;
    }




}
