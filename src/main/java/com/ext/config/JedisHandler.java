package com.ext.config;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ext.commons.enums.ResultCode;
import com.ext.commons.exception.HerinRuntimeException;
import com.ext.commons.utils.DateUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import redis.clients.jedis.*;
import redis.clients.jedis.exceptions.JedisException;
import redis.clients.jedis.params.geo.GeoRadiusParam;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

/**
 * description: redis工具类
 **/
//@Component
public class JedisHandler {

    private static final Logger logger = LogManager.getLogger(JedisHandler.class);
    private JedisPool jedisPool;
    private static final Long RELEASE_SUCCESS = 1L;
    private static final String NULL = "null";

    /**
     * 设置带有效期的字符串类型缓存
     * @param key key
     * @param value value
     * @param mins 过期时间 分钟数
     */
    public void set(String key, String value, int mins) {
        try (Jedis jedis = jedisPool.getResource()) {
            jedis.set(key, value);
            if (mins > 0) {
                jedis.expire(key, mins * 60);
            }
        } catch (Exception e) {
            logger.error("setKey: {}, value:{}, mins:{}, error:{} ", key, value, mins, e);
        }
    }

    /**
     * 设置一个过期时间的值，并根据后面的value做累加操作
     * @param key key
     * @param value value
     * @param mins 过期时间 分钟数
     */
    public void IncrBy(String key, Integer value, int mins) {
        try (Jedis jedis = jedisPool.getResource()) {
            jedis.incrBy(key,value);
            if (mins > 0) {
                jedis.expire(key, mins * 60);
            }
        } catch (Exception e) {
            logger.error("setKey: {}, value:{}, mins:{}, error:{} ", key, value, mins, e);
        }
    }

    /**
     * 设置带有效期的字符串类型缓存
     * seconds 过期时间：秒
     */
    public void set(String key, String value, Long seconds) {
        try (Jedis jedis = jedisPool.getResource()) {
            jedis.set(key, value);
            if (seconds > 0) {
                jedis.expire(key, seconds.intValue());
            }
        } catch (Exception e) {
            logger.error("setKey: {}, value:{}, seconds:{}, error:{} ", key, value, seconds, e);
        }
    }

    /**
     * 设置带有效期的字符串类型缓存
     * days 过期时间：天 + 随机值
     * 每次重新设定值后设置有效期
     */
    public void setNxDays(String key, String value, int days) {
        try (Jedis jedis = jedisPool.getResource()) {
            jedis.set(key, value);
            if (days > 0) {
                jedis.expire(key, getExpirePlusRandom(days));
            }
        } catch (Exception e) {
            logger.error("setNxDays key: {}, value:{}, days:{}, error:{} ", key, value, days, e);
        }
    }

    /**
     * 设置带字符串类型缓存
     * @param key   key
     * @param value value
     */
    public void set(String key, String value) {
        try (Jedis jedis = jedisPool.getResource()) {
            jedis.set(key, value);
        } catch (Exception e) {
            logger.error("setKey: {}, value:{}, error:{} ", key, value, e);
        }
    }

    /**
     * 根据key获取缓存数据
     */
    public String get(String key) {
        String value = "";
        try (Jedis jedis = jedisPool.getResource()) {
            value = jedis.get(key);
        } catch (Exception e) {
            logger.error("getKey: {}, error:{}", key, e);
        }
        return value;
    }

    /**
     * 获取模糊匹配的keys
     */
    public Set<String> getFuzzyKeys(String keyPattern) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.keys(keyPattern);
        } catch (Exception e) {
            logger.error("getFuzzyKeys: {}, error:{}", keyPattern, e);
        }
        return null;
    }

    /**
     * 是否存在key
     */
    public boolean existKey(String key) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.exists(key);
        } catch (Exception e) {
            logger.error("existKey: {}, error:{}", key, e);
        }
        return false;
    }

    /**
     * 根据key删除值
     */
    public Boolean del(String key) {
        try (Jedis jedis = jedisPool.getResource()) {
            Long result = jedis.del(key);
            if (result > 0) {
                return true;
            }
        } catch (Exception e) {
            logger.error("delKey: {}, error:{} ", key, e);
        }
        return false;
    }

    /**
     * 根据key从redis中获取
     * 如果已经跨日或跨月或跨年，删除redis中数据
     * 重新从数据库中读取
     */
    public String getResultFromRedis(String keyPrefix, String dateStr) {
        try (Jedis jedis = jedisPool.getResource()) {
            Set<String> fuzzyKeys = jedis.keys(keyPrefix + "*");
            if (fuzzyKeys != null && fuzzyKeys.size() > 0) {
                if (fuzzyKeys.contains(keyPrefix + dateStr)) {
                    return jedis.get(keyPrefix + dateStr);
                } else {
                    for (String fuzzyKey : fuzzyKeys) {
                        jedis.del(fuzzyKey);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("getResultFromRedis: keyPrefix:{}, dateStr:{}, error:{}", keyPrefix, dateStr, e);
        }
        return null;
    }

    /**
     * 添加分布式锁
     * @param key    key
     * @param value  value
     * @param second 过期时间 秒
     * @return 返回1设置成功，返回0失败
     */
    public long addLock(String key, String value, int second) {
        try (Jedis jedis = jedisPool.getResource()) {
            long lock = jedis.setnx(key, value);
            if (second > 0) {
                jedis.expire(key, second);
            }
            return lock;
        } catch (Exception e) {
            logger.error("setNxKey: {}, value:{}, second:{}, error:{} ", key, value, second, e);
        }
        return 0;
    }

    /**
     * 释放分布式锁
     */
    public boolean releaseLock(String key, String value) {
        boolean retFlag = false;
        try (Jedis redis = jedisPool.getResource()) {
            String script = "if redis.call('get', KEYS[1]) == ARGV[1] then return redis.call('del', KEYS[1]) else return 0 end";
            Object result = redis.eval(script, Collections.singletonList(key), Collections.singletonList(value));
            if (RELEASE_SUCCESS.equals(result)) {
                retFlag = true;
            }
        } catch (JedisException e) {
            logger.error("releaseLockKey: {}, value:{}, error:{} ", key, value, e);
        }
        return retFlag;
    }

    /**
     * 获取缓存中的hash类型的所有field字段
     */
    public Set<String> hashGetAllFields(String key) {
        try (Jedis jedis = jedisPool.getResource()) {
            Set<String> hkeys = jedis.hkeys(key);
            if (hkeys != null && hkeys.size() > 0) {
                return hkeys;
            }
        } catch (Exception e) {
            logger.error("hashGetAllFields: {}, error:{}", key, e);
           throw new HerinRuntimeException(ResultCode.REDIS_ERROR.getCode(),ResultCode.REDIS_ERROR.getMessage());
        }
        return null;
    }

    /**
     * 获取缓存中的hash类型是否存在某个field
     */
    public boolean hashExistField(String key, String field) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.hexists(key, field);
        } catch (Exception e) {
            logger.error("hashExistField: {}, field:{}, error:{}", key, field, e);
           throw new HerinRuntimeException(ResultCode.REDIS_ERROR.getCode(),ResultCode.REDIS_ERROR.getMessage());
        }
    }

    /**
     * 删除缓存中hash中的某个field值
     */
    public Boolean hashDel(String key, String field) {
        try (Jedis jedis = jedisPool.getResource()) {
            Long result = jedis.hdel(key, field);
            return result > 0;
        } catch (Exception e) {
            logger.error("hashDel: key:{}, field:{} error:{}", key, field, e);
           throw new HerinRuntimeException(ResultCode.REDIS_ERROR.getCode(),ResultCode.REDIS_ERROR.getMessage());
        }
    }

    /**
     * 删除缓存中hash中的一批field集合
     */
    public Boolean hashDel(String key, List<String> fields) {
        try (Jedis jedis = jedisPool.getResource()) {
            Long result = jedis.hdel(key, fields.toArray(new String[0]));
            return result > 0;
        } catch (Exception e) {
            logger.error("hashDel: key:{}, fields:{} error:{}", key, fields, e);
           throw new HerinRuntimeException(ResultCode.REDIS_ERROR.getCode(),ResultCode.REDIS_ERROR.getMessage());
        }
    }

    /**
     * 获取缓存中的hash类型转换为指定的类
     */
    public <T> T hashGet(String key, String field, Class<T> clazz) {
        try (Jedis jedis = jedisPool.getResource()) {
            String value = jedis.hget(key, field);
            if (value != null && !NULL.equals(value)) {
                return JSON.parseObject(value, clazz);
            }
        } catch (Exception e) {
            logger.error("hashGetKey: {}, error:{}", key, e);
           throw new HerinRuntimeException(ResultCode.REDIS_ERROR.getCode(),ResultCode.REDIS_ERROR.getMessage());
        }
        return null;
    }

    /**
     * 获取缓存中的hash类型转换为指定的类
     * @return
     */
    public <T> String hashGet(String key, String field) {
        try (Jedis jedis = jedisPool.getResource()) {
            String value = jedis.hget(key, field);
            if (value != null && !NULL.equals(value)) {
                return value;
            }
        } catch (Exception e) {
            logger.error("hashGetKey: {}, error:{}", key, e);
            throw new HerinRuntimeException(ResultCode.REDIS_ERROR.getCode(),ResultCode.REDIS_ERROR.getMessage());
        }
        return null;
    }

    public <T> List<T> hashGetToList(String key, String field, Class<T> clazz) {
        try (Jedis jedis = jedisPool.getResource()) {
            String value = jedis.hget(key, field);
            if (value != null && !NULL.equals(value)) {
                return JSONObject.parseArray(value, clazz);
            }
        } catch (Exception e) {
            logger.error("hashGetKey: {}, error:{}", key, e);
            throw new HerinRuntimeException(ResultCode.REDIS_ERROR.getCode(),ResultCode.REDIS_ERROR.getMessage());
        }
        return null;
    }

    /**
     * 获取缓存中的hash类型转换为指定的类
     */
    public Map<String, String> hashGetAll(String key) {
        try (Jedis jedis = jedisPool.getResource()) {
            Map<String, String> result = jedis.hgetAll(key);
            if (result != null) {
                return result;
            }
        } catch (Exception e) {
            logger.error("hashGetAll: {}, error:{}", key, e);
            throw new HerinRuntimeException(ResultCode.REDIS_ERROR.getCode(),ResultCode.REDIS_ERROR.getMessage());
        }
        return null;
    }

    /**
     * 获取缓存中指定fields集合的数据
     */
    public List<String> hashGetByFields(String key, List<String> fields) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.hmget(key, fields.toArray(new String[0]));
        } catch (Exception e) {
            logger.error("hashGetByFields: {}, error:{}", key, e);
           throw new HerinRuntimeException(ResultCode.REDIS_ERROR.getCode(),ResultCode.REDIS_ERROR.getMessage());
        }
    }

    /**
     * 删除缓存中指定的fields
     */
    public void hashDelExcludeFields(String key, Set<String> excludeFields) {
        try (Jedis jedis = jedisPool.getResource()) {
            Set<String> hkeys = jedis.hkeys(key);
            HashSet<String> result = new HashSet<>(hkeys);
            result.removeAll(excludeFields);
            if (result.size() > 0) {
                jedis.hdel(key, result.toArray(new String[0]));
            }

        } catch (Exception e) {
            logger.error("hashGetByFields: {}, error:{}", key, e);
            throw new HerinRuntimeException(ResultCode.REDIS_ERROR.getCode(),ResultCode.REDIS_ERROR.getMessage());
        }
    }


    /**
     * 增加hash类型缓存
     */
    public void hashAdd(String key, String field, String obj) {
        try (Jedis jedis = jedisPool.getResource()) {
            jedis.hset(key, field, obj);
        } catch (Exception e) {
            logger.error("hashAddKey: {}, error:{}", key, e);
            throw new HerinRuntimeException(ResultCode.REDIS_ERROR.getCode(), ResultCode.REDIS_ERROR.getMessage());
        }
    }

    /**
     * 增加hash类型缓存, 带有效期
     */
    public void hashAdd(String key, String field, String obj, int minutes) {
        try (Jedis jedis = jedisPool.getResource()) {
            jedis.hset(key, field, obj);
            if (minutes > 0) {
                jedis.expire(key, minutes * 60);
            }
        } catch (Exception e) {
            logger.error("hashAddKey: {}, minutes:{},  error:{}", key, minutes, e);
            throw new HerinRuntimeException(ResultCode.REDIS_ERROR.getCode(),ResultCode.REDIS_ERROR.getMessage());
        }
    }

    /**
     * 批量增加hash类型缓存的field和value
     */
    public void hashAdd(String key, Map<String, String> fieldValue) {
        try (Jedis jedis = jedisPool.getResource()) {
            String result = jedis.hmset(key, fieldValue);
        } catch (Exception e) {
            logger.error("hashAdd: {}, fieldValue:{},  error:{}", key, JSON.toJSONString(fieldValue), e);
            throw new HerinRuntimeException(ResultCode.REDIS_ERROR.getCode(),ResultCode.REDIS_ERROR.getMessage());
        }
    }




    /**
     * 缓存地理位置信息
     */
    public void geoAdd(String key, double x, double y, String value) {
        try (Jedis jedis = jedisPool.getResource()) {
            jedis.geoadd(key, x, y, value);
        } catch (Exception e) {
            logger.error("geoAddKey: {}, x:{}, y:{}, error:{}", key, x, y, e);
            throw new HerinRuntimeException(ResultCode.REDIS_ERROR.getCode(),ResultCode.REDIS_ERROR.getMessage());
        }
    }

    /**
     * 缓存地理位置信息
     */
    public void geoAdd(String key, double x, double y, String value, Long timestamp) {
        try (Jedis jedis = jedisPool.getResource()) {
            jedis.geoadd(key, x, y, value);
            if (timestamp > 0) {
                jedis.expireAt(key, timestamp);
            }
        } catch (Exception e) {
            logger.error("geoAddKey: {}, x:{}, y:{}, error:{}", key, x, y, e);
            throw new HerinRuntimeException(ResultCode.REDIS_ERROR.getCode(),ResultCode.REDIS_ERROR.getMessage());
        }
    }

    /**
     * 通过给定的坐标和距离(km)获取范围类其它的坐标信息
     */
    public List<GeoRadiusResponse> radiusGeo(String key, double x, double y, double distance, int count) {
        try (Jedis jedis = jedisPool.getResource()) {
            GeoRadiusParam params = GeoRadiusParam.geoRadiusParam();
            params.withDist();
            params.sortAscending();
            params.count(count);
            return jedis.georadius(key, x, y, distance, GeoUnit.M, params);
        } catch (Exception e) {
            logger.error("radiusGeoKey: {}, error:{}", key, e);
        }
        return new ArrayList<>();
    }

    /**
     * 获取两个坐标之间的距离
     * @param key     key
     * @param member1 geoadd的value 可添加多个
     * @param member2 geoadd的value
     * @param unit    单位：米、千米、英里、英尺
     * @return 距离
     */
    public Double geoDist(String key, String member1, String member2, GeoUnit unit) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.geodist(key, member1, member2, unit);
        } catch (Exception e) {
            logger.error("geoDistKey: {}, error:{}", key, e);
        }
        return null;
    }

    /**
     * list添加元素维持固定长度，右侧入栈，左侧出栈，时间最早的排在最前面
     */
    public void rPushLimit(String key, String value, int limit) {
        try (Jedis jedis = jedisPool.getResource()) {
            jedis.rpush(key, value);
            jedis.ltrim(key, -limit, -1);
        } catch (Exception e) {
            logger.error("rPushLimit key: {}, error:{}", key, e);
        }
    }

    /**
     * 获取list从左到右所有数据
     */
    public List<String> getAllListLR(String key) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.lrange(key, 0, -1);
        } catch (Exception e) {
            logger.error("rPushLimit key: {}, error:{}", key, e);
        }
        return null;
    }


    /**
     * set 添加
     * @return
     */
    public <T> String sAdd(String key, String field) {
        try (Jedis jedis = jedisPool.getResource()) {
            jedis.sadd(key, field);
        } catch (Exception e) {
            logger.error("hashGetKey: {}, error:{}", key, e);
            throw new HerinRuntimeException(ResultCode.REDIS_ERROR.getCode(),ResultCode.REDIS_ERROR.getMessage());
        }
        return null;
    }

    public <T> Set<String> smembers(String key) {
        try (Jedis jedis = jedisPool.getResource()) {
            Set<String> set = jedis.smembers(key);
            return set;
        } catch (Exception e) {
            logger.error("hashGetKey: {}, error:{}", key, e);
            throw new HerinRuntimeException(ResultCode.REDIS_ERROR.getCode(),ResultCode.REDIS_ERROR.getMessage());
        }

    }

    /**
     * 向zset添加元素days天的有效期
     * @param key   zset的key
     * @param score 每一元素的分值
     * @param value 每一元素的值
     * @param days  有效期, 天; -1不设置有效期
     */
    public void zadd(String key, Double score, String value, int days) {
        try (Jedis jedis = jedisPool.getResource()) {
            Boolean exists = jedis.exists(key);
            jedis.zadd(key, score, value);
            if (!exists && days > 0) {
                jedis.expire(key, getExpirePlusRandom(days));
            }
        } catch (Exception e) {
            logger.error("zadd Key: {}, score:{}, days:{} error:{}", key, score, days, e);
            throw new HerinRuntimeException(ResultCode.REDIS_ERROR.getCode(),ResultCode.REDIS_ERROR.getMessage());
        }
    }

    /**
     * 批量添加zset
     * @param key    zset的key
     * @param scores 多个元素的score集合
     * @param values 多个元素的值集合
     * @param <T>    多个元素的class类型
     */
    public <T> void zadd(String key, List<Double> scores, List<T> values) {
        try (Jedis jedis = jedisPool.getResource()) {
            Map<String, Double> scoreMembers = new HashMap<>();
            for (int i = 0; i < scores.size(); i++) {
                scoreMembers.put(JSON.toJSONString(values.get(i)), scores.get(i));
            }
            if (jedis.exists(key)) {
                jedis.del(key);
            }
            jedis.zadd(key, scoreMembers);
        } catch (Exception e) {
            logger.error("zadd scores values Key: {}, error:{}", key, e);
            throw new HerinRuntimeException(ResultCode.REDIS_ERROR.getCode(),ResultCode.REDIS_ERROR.getMessage());
        }
    }

    /**
     * 将有效期加随机时间
     * 1 days表示从现在开始到明天00:00:00秒 + 随机时间（300-6000秒）
     */
    private static Integer getExpirePlusRandom(int days) {
        //明天00:00
        LocalDateTime until = LocalDateTime.of(LocalDate.now().plusDays(days), LocalTime.MIN);
        // 加随机数
        until = until.plus(ThreadLocalRandom.current().nextInt(300, 6000), ChronoUnit.SECONDS);
        // 当前时间到明天+随机数的时间差，单位：秒
        Long expire = LocalDateTime.now().until(until, ChronoUnit.SECONDS);
        return expire.intValue();
    }

    /**
     * 根据天数获取有效期，不加随机值
     * 1 days表示从现在开始到明天00:00:00秒
     */
    private static Integer getExpirePlus(int days) {
        //明天00:00
        LocalDateTime until = LocalDateTime.of(LocalDate.now().plusDays(days), LocalTime.MIN);
        // 当前时间到明天+随机数的时间差，单位：秒
        Long expire = LocalDateTime.now().until(until, ChronoUnit.SECONDS);
        return expire.intValue();
    }

    /**
     * 获取zSet所有集合
     * @param key   zSet的key
     * @param clazz 取出的元素类型
     */
    public <T> List<T> zSetGetAll(String key, Class<T> clazz) {
        try (Jedis jedis = jedisPool.getResource()) {
            Iterator<String> iterator = jedis.zrange(key, 0, -1).iterator();
            List<T> list = new ArrayList<>();
            while (iterator.hasNext()) {
                list.add(JSON.parseObject(iterator.next(), clazz));
            }
            return list;
        } catch (Exception e) {
            logger.error("zSetGetAll: {}, error:{}", key, e);
            throw new HerinRuntimeException(ResultCode.REDIS_ERROR.getCode(),ResultCode.REDIS_ERROR.getMessage());
        }
    }


    /**
     * 返回集合中元素的数量
     */
    public Long zSetGetAllNum(String key) {
        try (Jedis jedis = jedisPool.getResource()) {
            if (!jedis.exists(key)){
                return null;
            }
            return jedis.zcard(key);
        } catch (Exception e) {
            logger.error("zSetGet: {}, error:{}", key, e);
            throw new HerinRuntimeException(ResultCode.REDIS_ERROR.getCode(),ResultCode.REDIS_ERROR.getMessage());
        }
    }

    public Long hashGetAllNum(String key) {
        try (Jedis jedis = jedisPool.getResource()) {
            if (!jedis.exists(key)) {
                return null;
            }
            return jedis.hlen(key);
        } catch (Exception e) {
            logger.error("zSetGet: {}, error:{}", key, e);
            throw new HerinRuntimeException(ResultCode.REDIS_ERROR.getCode(),ResultCode.REDIS_ERROR.getMessage());
        }
    }

    /**
     * 获取包括score在内的所有zset值
     */
    public Set<Tuple> zSetGetAllWithScore(String key) {
        try (Jedis jedis = jedisPool.getResource()) {
            if (!jedis.exists(key)) {
                return null;
            }
            return jedis.zrangeWithScores(key, 0, -1);
        } catch (Exception e) {
            logger.error("zSetGet: {}, error:{}", key, e);
            throw new HerinRuntimeException(ResultCode.REDIS_ERROR.getCode(),ResultCode.REDIS_ERROR.getMessage());
        }
    }


    /**
     * 获取zset最后一条数据
     * @param key zset的key
     * @return 最后一条元素
     */
    public String zLast(String key) {
        try (Jedis jedis = jedisPool.getResource()) {
            if (jedis.exists(key)) {
                Set<String> results = jedis.zrevrange(key, 0, 0);
                if (results != null && results.size() > 0) {
                    return results.iterator().next();
                }
            }
        } catch (Exception e) {
            logger.error("zaddKey: {}, error:{}", key, e);
            throw new HerinRuntimeException(ResultCode.REDIS_ERROR.getCode(),ResultCode.REDIS_ERROR.getMessage());
        }
        return null;
    }


    public Set<String> zFirst(String key, long start, long end) {
        try (Jedis jedis = jedisPool.getResource()) {
            if (jedis.exists(key)) {
                Set<String> results = jedis.zrange(key, start, end);
                if (results != null && results.size() > 0) {
                    return results;
                }
            }
        } catch (Exception e) {
            logger.error("zaddKey: {}, error:{}", key, e);
            throw new HerinRuntimeException(ResultCode.REDIS_ERROR.getCode(),ResultCode.REDIS_ERROR.getMessage());
        }
        return null;
    }


    /**
     * 获取指定区间的元素
     * @param key   zset的key
     * @param start 开始的索引
     * @param end   结束的索引
     * @return 返回指定区间的元素
     */
    public <T> List<T> zGetByIndex(String key, Integer start, Integer end, Class<T> clazz) {
        try (Jedis jedis = jedisPool.getResource()) {
            if (jedis.exists(key)) {
                Iterator<String> iterator = jedis.zrange(key, start.longValue(), end.longValue()).iterator();
                List<T> list = new ArrayList<>();
                while (iterator.hasNext()) {
                    list.add(JSON.parseObject(iterator.next(), clazz));
                }
                return list;
            }
        } catch (Exception e) {
            logger.error("zaddKey: {}, error:{}", key, e);
            throw new HerinRuntimeException(ResultCode.REDIS_ERROR.getCode(),ResultCode.REDIS_ERROR.getMessage());
        }
        return null;
    }

    /**
     * 移除有序集zset中的一个或多个成员
     */
    public void zRem(String key, String... members) {
        try (Jedis jedis = jedisPool.getResource()) {
            jedis.zrem(key, members);
        } catch (Exception e) {
            logger.error("zRemKey: {}, error:{}", key, e);
            throw new HerinRuntimeException(ResultCode.REDIS_ERROR.getCode(),ResultCode.REDIS_ERROR.getMessage());
        }
    }

    /**
     * 删除哈希表 key 中的一个或多个指定字段，不存在的字段将被忽略
     */
    public void hDel(String key, String... fields) {
        try (Jedis jedis = jedisPool.getResource()) {
            jedis.hdel(key, fields);
        } catch (Exception e) {
            logger.error("hDelKey: {}, error:{}", key, e);
            throw new HerinRuntimeException(ResultCode.REDIS_ERROR.getCode(),ResultCode.REDIS_ERROR.getMessage());
        }
    }

    /**
     * 对比值，将较大的值设置为value
     */
    public void setMax(String key, String value) {
        this.setMaxOrMin(key, value, 1);
    }

    /**
     * 对比值，将较小的值设置为value
     */
    public void setMin(String key, String value) {
        this.setMaxOrMin(key, value, -1);
    }

    /**
     * 对比值，将较大或较小的值设置为value
     */
    private void setMaxOrMin(String key, String value, Integer maxOrMin) {
        try (Jedis redis = jedisPool.getResource()) {
            Boolean exists = redis.exists(key);
            if (!exists) {
                redis.set(key, value);
            } else {
                String script = null;
                if (maxOrMin == 1) {
                    script = "if tonumber(redis.call('get', KEYS[1])) < tonumber(ARGV[1]) then return redis.call('set', KEYS[1], ARGV[1]) else return 'OK' end";
                }
                if (maxOrMin == -1) {
                    script = "if tonumber(redis.call('get', KEYS[1])) > tonumber(ARGV[1]) then return redis.call('set', KEYS[1], ARGV[1]) else return 'OK' end";
                }
                redis.eval(script, Collections.singletonList(key), Collections.singletonList(value));
            }
        } catch (JedisException e) {
            logger.error("setMaxOrMin Key: {}, value:{}, error:{} ", key, value, e);
        }
    }

    /**
     * 对比值，将较大的值设置为value, 并设置有效期 + 随机值
     */
    public void setMax(String key, String value, int days) {
        this.setMaxOrMin(key, value, days, 1);
    }

    /**
     * 对比值，将较大的值设置为value, 并设置有效期 + 随机值
     */
    public void setMin(String key, String value, int days) {
        this.setMaxOrMin(key, value, days, -1);
    }

    /**
     * 对比值，将较大或较小的值设置为value, 并设置有效期 + 随机值
     */
    private void setMaxOrMin(String key, String value, int days, Integer maxOrMin) {
        try (Jedis redis = jedisPool.getResource()) {
            Boolean exists = redis.exists(key);
            if (!exists) {
                redis.set(key, value);
                if (days > 0) {
                    redis.expire(key, getExpirePlusRandom(days));
                }
            } else {
                String script = null;
                if (maxOrMin == 1) {
                    script = "local t = redis.call('ttl',KEYS[1]);if tonumber(redis.call('get', KEYS[1])) < tonumber(ARGV[1]) then redis.call('set', KEYS[1], ARGV[1]) redis.call('expire',KEYS[1],t) else return 'OK' end";
                }
                if (maxOrMin == -1) {
                    script = "local t = redis.call('ttl',KEYS[1]);if tonumber(redis.call('get', KEYS[1])) > tonumber(ARGV[1]) then redis.call('set', KEYS[1], ARGV[1]) redis.call('expire',KEYS[1],t) else return 'OK' end";
                }
                redis.eval(script, Collections.singletonList(key), Collections.singletonList(value));
            }
        } catch (JedisException e) {
            logger.error("setMaxOrMinWithExpire Key: {}, value:{}, error:{} ", key, value, e);
        }
    }

    /**
     * 对比值，将较大的值设置为value, 并设置有效期
     */
    public void setMaxWithoutRandom(String key, String value, int days) {
        try (Jedis redis = jedisPool.getResource()) {
            Boolean exists = redis.exists(key);
            if (!exists) {
                redis.set(key, value);
                if (days > 0) {
                    redis.expire(key, getExpirePlus(days));
                }
            } else {
                String script = "local t = redis.call('ttl',KEYS[1]);if tonumber(redis.call('get', KEYS[1])) < tonumber(ARGV[1]) then redis.call('set', KEYS[1], ARGV[1]) redis.call('expire',KEYS[1],t) else return 'OK' end";
                redis.eval(script, Collections.singletonList(key), Collections.singletonList(value));
            }
        } catch (JedisException e) {
            logger.error("setMax Key: {}, value:{}, error:{} ", key, value, e);
        }
    }

    /**
     * 生成编号，格式为： 当日日期yyyyMMdd + "0123"
     */
    public String generateIdByToday(String key) {
        try (Jedis redis = jedisPool.getResource()) {
            String script = "local c = redis.call('INCRBY', KEYS[1], 1);redis.call('expire', KEYS[1], ARGV[1]);return KEYS[2]..string.format('%04d', c)";
            List<String> keys = Arrays.asList(key, DateUtils.formatDate(LocalDateTime.now(), "yyyyMMdd"));
            return (String) redis.eval(script, keys, Collections.singletonList(getExpirePlusRandom(1).toString()));
        } catch (JedisException e) {
            logger.error("generate Key: {}, error:{} ", key, e);
        }
        return null;
    }

    /**
     * 对比zset中最后一条数据
     * 如果新插入的这条数据value的前缀与最后一条相同，则删除最后一条数据，并将新的这一条插入；
     * 否则直接插入
     */
    public void zAddReplaceSameValue(String key, Double score, String value, int days) {
        try (Jedis redis = jedisPool.getResource()) {
            String script = "local len=redis.call('zcard', KEYS[1]);local c=redis.call('zrange',KEYS[1], -1, -1);if(len > 1) then local b=string.find(c[1], '_');local d=string.sub(c[1], 0, b-1);if(d==ARGV[1]) then redis.call('zrem',KEYS[1], c[1]) redis.call('zadd', KEYS[1], ARGV[2], ARGV[3]) return redis.call('expire', KEYS[1], ARGV[4]) else redis.call('zadd', KEYS[1], ARGV[2], ARGV[3]) return redis.call('expire', KEYS[1], ARGV[4]) end else redis.call('zadd', KEYS[1], ARGV[2], ARGV[3]) return redis.call('expire', KEYS[1], ARGV[4]) end";
            List<String> args = new ArrayList<>();
            args.add(value.split("_")[0]);
            args.add(String.valueOf(score));
            args.add(value);
            args.add(getExpirePlusRandom(days).toString());
            redis.eval(script, Collections.singletonList(key), args);
        } catch (JedisException e) {
            logger.error("zAddReplaceSameValue Key: {}, error:{} ", key, e);
        }
    }

    /**
     * 对比zset中最后一条数据
     * 如果新插入的这条数据时间大于上一条3分钟，则插入新数据，否则不操作
     */
    public void zAddFixedInterval(String key, Double score, String value, int days) {
        try (Jedis redis = jedisPool.getResource()) {
            String script = "local len=redis.call('zcard', KEYS[1]);local c=redis.call('zrange',KEYS[1], -1, -1,'WITHSCORES');if(len > 0) then local d=c[2]+180;if(tonumber(ARGV[1])>=d) then redis.call('zadd', KEYS[1], ARGV[1], ARGV[2]) return redis.call('expire', KEYS[1], ARGV[3]) else return 0 end else redis.call('zadd', KEYS[1], ARGV[1], ARGV[2]) return redis.call('expire', KEYS[1], ARGV[3]) end";
            List<String> args = new ArrayList<>();
            args.add(String.valueOf(score));
            args.add(value);
            args.add(getExpirePlusRandom(days).toString());
            redis.eval(script, Collections.singletonList(key), args);
        } catch (JedisException e) {
            logger.error("zAddReplaceSameValue Key: {}, error:{} ", key, e);
        }
    }

    @Autowired
    public void setJedisPool(JedisPool jedisPool) {
        this.jedisPool = jedisPool;
    }
}

