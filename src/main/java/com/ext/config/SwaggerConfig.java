package com.ext.config;

import cn.hutool.core.date.DateUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.Date;

@Configuration
//@EnableSwagger2
//@EnableKnife4j
public class SwaggerConfig {

    private String host;//自定义host

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.ext.controller"))//扫描包
                .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))//扫描在API注解的contorller
                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))//扫描带ApiOperation注解的方法
                .paths(PathSelectors.any())
                .build().host(host);//自定义host
    }

    private ApiInfo apiInfo() {
        String version = DateUtil.format(new Date(),"yyyyMMdd.HHmmss");
        return new ApiInfoBuilder()
                .title("权限系统")
                .description("包括用户，部门，角色，权限模块")
                .version("v2."+version)
                .license("Apache License Version 2.0")
                .licenseUrl("https//localhost:8080/doc.html")
                .contact(new Contact("zyj", "", "248342050@qq.com"))
                .build();
    }
}
