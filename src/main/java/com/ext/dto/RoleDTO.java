package com.ext.dto;


import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoleDTO {
    @ApiModelProperty(value = "id",required = true)
    @NotNull(message = "角色ID不能为空")
    private Integer userId;
    @ApiModelProperty(value = "角色集合",required = true)
    private List<Integer> roleIdList;
}
