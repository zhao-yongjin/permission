package com.ext.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PassDto {
    @NotBlank(message = "新密码不能为空")
    @NotNull(message = "角色ID不能为空")
    private String password;

    @NotBlank(message = "旧密码不能为空")
    private String currentPass;
}
