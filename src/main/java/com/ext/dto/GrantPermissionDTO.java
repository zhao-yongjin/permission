package com.ext.dto;


import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GrantPermissionDTO {
    @ApiModelProperty(value = "id",required = true)
    @NotNull(message = "角色ID不能为空")
    private Integer roleId;
    @ApiModelProperty(value = "角色集合",required = true)
    private List<Integer> permissionIdList;


}
