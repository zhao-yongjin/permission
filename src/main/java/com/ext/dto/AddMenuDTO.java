package com.ext.dto;


import io.swagger.annotations.ApiModelProperty;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddMenuDTO {
    @ApiModelProperty(value = "菜单权限",required = true)
    private String perms;
    @ApiModelProperty(value = "菜单权限名称(必传)",required = true)
    private String permsName;
    @ApiModelProperty(value = "菜单url",required = true)
    private String path;
}
