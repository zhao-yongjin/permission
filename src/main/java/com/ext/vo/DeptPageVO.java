package com.ext.vo;

import cn.hutool.core.util.ObjectUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DeptPageVO {
    // 当前页
    @ApiModelProperty("当前页")
    private int currentPage;
    // 页容量
    @ApiModelProperty("页容量")
    private int pageSize;
    public Integer getPageSize() {
        if (ObjectUtil.isNotEmpty(this.pageSize) || this.pageSize < 1) {
            return 10;
        }
        return pageSize;
    }

    public Integer getCurrentPage() {
        if (ObjectUtil.isNotEmpty(this.currentPage) || this.currentPage < 1) {
            return 1;
        }
        return currentPage;
    }
    // 模糊查询
    @ApiModelProperty("模糊查询(部门名称)")
    private String dname;
}
