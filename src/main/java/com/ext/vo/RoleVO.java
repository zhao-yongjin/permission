package com.ext.vo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("角色")
public class RoleVO {
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "编码")
    private Integer id;
    @ApiModelProperty("角色名称")
    private String rname;
    @ApiModelProperty("角色描述")
    private String rdesc;
    @ApiModelProperty("创建人姓名")
    private String createName;
    @ApiModelProperty("创建时间")
    @TableField(fill = FieldFill.INSERT) //创建时自动填充
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
}
