package com.ext.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("部门修改")
public class UpDeptVO {
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "编码",required = true)
    private Integer id;

    @ApiModelProperty("部门名称")
    private String dname;


    @ApiModelProperty("部门描述")
    private String remark;
}
