package com.ext.vo;

import cn.hutool.core.util.ObjectUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserPageVO {
    // 当前页
    @ApiModelProperty("当前页")
    private int currentPage=1;
    // 页容量
    @ApiModelProperty("页容量")
    private int pageSize=10;

    public Integer getSize() {
        if (ObjectUtil.isNotEmpty(this.pageSize) || this.pageSize < 1) {
            return 10;
        }
        return pageSize;
    }

    public Integer getCurrent() {
        if (ObjectUtil.isNotEmpty(this.currentPage) || this.currentPage < 1) {
            return 1;
        }
        return currentPage;
    }
    // 模糊查询
    @ApiModelProperty("模糊查询(权限名称)")
    private String username;
}
