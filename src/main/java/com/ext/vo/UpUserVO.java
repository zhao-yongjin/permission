package com.ext.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("用户修改")
public class UpUserVO {
    @ApiModelProperty(value = "id",required = true)
    @TableId(type = IdType.AUTO)
    private Long id;
    @ApiModelProperty("姓名")
    private String username;
    @ApiModelProperty("用户部门名称")
    private String userDeptName;
    @ApiModelProperty("用户角色名称")
    private String userRoleName;
    @ApiModelProperty("部门id")
    private Integer deptId;
}
