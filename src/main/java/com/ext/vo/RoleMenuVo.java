package com.ext.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("角色授权")
public class RoleMenuVo {
    @ApiModelProperty("角色id")
    private Integer roleId;
    @ApiModelProperty("权限id集合")
    private List<Integer> menuIdList;
}
