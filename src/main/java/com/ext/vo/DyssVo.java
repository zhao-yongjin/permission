package com.ext.vo;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("xsxa")
public class DyssVo {
    /**
     * 记录地音事件的通道号
     */
    private String channelNum;

    /**
     * 有效事件能量
     */
    private String validEventEnergy;
}
