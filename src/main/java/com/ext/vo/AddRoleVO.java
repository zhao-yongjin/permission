package com.ext.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("权限新增")
public class AddRoleVO {
    @ApiModelProperty(value = "角色名称",required = true)
    private String rname;
    @ApiModelProperty("角色描述")
    private String rdesc;
//    @ApiModelProperty(value = "创建人姓名",required = true)
//    private String createName;
}
