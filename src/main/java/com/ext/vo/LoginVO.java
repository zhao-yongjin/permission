package com.ext.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("登录")
public class LoginVO {
    @ApiModelProperty(value = "姓名",required = true)
    private String username;
    @ApiModelProperty(value = "密码",required = true)
    private String password;

}
