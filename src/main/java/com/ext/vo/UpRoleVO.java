package com.ext.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("角色修改")
public class UpRoleVO {
    @ApiModelProperty(value = "角色id",required = true)
    @TableId(type = IdType.AUTO)
    private Integer id;
    @ApiModelProperty("角色名称")
    private String rname;
    @ApiModelProperty("角色描述")
    private String rdesc;
}
