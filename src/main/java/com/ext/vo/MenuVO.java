package com.ext.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("权限")
public class MenuVO {
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "编码",required =true)
    private Integer id;
    @ApiModelProperty("菜单权限名称")
    private String permsName;
    @ApiModelProperty("菜单url")
    private String path;
    //查询时不显示此字段
    @ApiModelProperty("逻辑删除")
    @TableField(select = false)
    private  Integer deleted;
}
