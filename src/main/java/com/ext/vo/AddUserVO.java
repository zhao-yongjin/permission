package com.ext.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@ApiModel("用户新增")
@NoArgsConstructor
@AllArgsConstructor
public class AddUserVO implements Serializable {
    @ApiModelProperty(value = "姓名",required = true)
    private String username;
    @ApiModelProperty("性别")
    private Integer sex;
    @ApiModelProperty("邮箱")
    private String email;
    @ApiModelProperty(value = "用户部门名称",required = true)
    private String userDeptName;
    @ApiModelProperty(value = "用户角色名称",required =true)
    private String userRoleName;
    @ApiModelProperty(value = "部门id",required = true)
    private Integer deptId;
//    @ApiModelProperty(value = "创建人姓名",required = true)
//    private String createName;
    //    @ApiModelProperty(value = "密码",required = true)
//    private String password;
    //    @ApiModelProperty("头像")
//    private String imgPath;
}
