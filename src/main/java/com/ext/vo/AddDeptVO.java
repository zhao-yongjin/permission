package com.ext.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;


@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("部门新增")
public class AddDeptVO {
    @ApiModelProperty(value = "部门名称",required = true)
    private String dname;

    @ApiModelProperty("部门描述")
    private String remark;


//    @ApiModelProperty(value = "创建人姓名",required = true)
//    private String createName;




}
