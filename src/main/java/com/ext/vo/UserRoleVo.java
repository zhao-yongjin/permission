package com.ext.vo;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("用户角色")
public class UserRoleVo {

    private Integer roleId;

    private List<Integer> userIdList;
}
