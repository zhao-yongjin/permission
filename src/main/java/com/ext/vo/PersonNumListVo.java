package com.ext.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
public class PersonNumListVo {


    @ApiModelProperty("时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:00:00",timezone="GMT+8")
    private Date createTime;

    @ApiModelProperty("描述信息")
    private String s;

    @ApiModelProperty("人数")
    private String personNum;
}
