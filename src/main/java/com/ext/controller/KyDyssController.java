package com.ext.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 地音监测系统实时数据 前端控制器
 * </p>
 *
 * @author 我的公众号：MarkerHub
 * @since 2022-12-07
 */
@RestController
@RequestMapping("/ky-dyss")
public class KyDyssController {

}
