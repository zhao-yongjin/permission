package com.ext.controller;


import com.ext.commons.utils.R;
import com.ext.service.TDeptHistoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 我的公众号：MarkerHub
 * @since 2022-12-07
 */
@RestController
@RequestMapping("/t-dept-history")
@Api(tags = "测试管理")
public class TDeptHistoryController {
    @Autowired
    private TDeptHistoryService deptHistoryService;

    @GetMapping({"/tes1"})
    @ApiOperation("获取用户信息")
    public R<List> list() {
        return  R.data(deptHistoryService.findAll());
    }



}
