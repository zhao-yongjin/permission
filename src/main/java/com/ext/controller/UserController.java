package com.ext.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ext.commons.utils.R;
import com.ext.dto.RoleDTO;
import com.ext.service.UserService;
import com.ext.vo.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
@Api(tags = "用户管理")
public class UserController {
    @Autowired
    private UserService userService;


    @GetMapping({"/list"})
    @PreAuthorize("hasAuthority('api:user:list')")
    @ApiOperation("获取用户全部信息")
    public R<List> list() {
        return  R.data(userService.findAll());
    }

    @ApiOperation("分页模糊查询用户信息")
    @PostMapping({"/findData"})
    @PreAuthorize("hasAuthority('api:user:findData')")
    public R getUserList(@RequestBody(required = false) UserPageVO userPageVO){
        Page list = userService.getUserList(userPageVO);
        return R.data(list);
    }

    @PostMapping({"/save"})
    @PreAuthorize("hasAuthority('api:user:save')")
    @ApiOperation("保存用户信息")
    public R<String> saveUser(@RequestBody AddUserVO userVO) {
       return R.data(userService.inseterUser(userVO));
    }

    @PostMapping({"/update"})
    @PreAuthorize("hasAuthority('api:user:update')")
    @ApiOperation("修改用户信息")
    public R<String> update(@RequestBody UpUserVO userVO) {
        return R.data(userService.updateUser(userVO));
    }

    @Transactional
    @GetMapping({"/remove"})
    @ApiOperation("根据id删除用户信息")
    @PreAuthorize("hasAuthority('api:role:remove')")
    public R<String> remove(@RequestParam Integer id) {
        return R.data(userService.deleteById(id));
    }

    @PostMapping({"/updatepwd"})
    @ApiOperation("修改用户密码")
    @PreAuthorize("hasAuthority('api:user:uppwd')")
    public R<String> updatePwd(@RequestBody LoginVO userVO){
        return R.data(userService.updatePwd(userVO));
    }


    @PostMapping("/setRole")
    @ApiOperation("给用户设置角色")
    public R<String> setRolelist(@Validated @RequestBody RoleDTO roleDTO) {
        return R.data(userService.setRolelist(roleDTO));

    }


}
