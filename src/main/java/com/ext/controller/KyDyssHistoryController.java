package com.ext.controller;


import com.ext.commons.utils.R;
import com.ext.service.KyDyssHistoryService;
import com.ext.service.TDeptHistoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 地音监测系统实时数据 前端控制器
 * </p>
 *
 * @author 我的公众号：MarkerHub
 * @since 2022-12-07
 */
@RestController
@RequestMapping("/ky-dyss-history")
@Api(tags = "地音管理")
public class KyDyssHistoryController {
    @Autowired
    private KyDyssHistoryService kyDyssHistoryService;

    @GetMapping({"/tes1"})
    @ApiOperation("获取用户信息")
    public R<List> list() {
        return  R.data(kyDyssHistoryService.findAll());
    }
}
