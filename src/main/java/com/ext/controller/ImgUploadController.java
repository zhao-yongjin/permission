package com.ext.controller;

import com.ext.commons.utils.R;
import com.ext.service.ImgService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@CrossOrigin
@RestController
@Api(tags = {"文件上传"})
@Slf4j
public class ImgUploadController {
    @Autowired
    private ImgService imgService;

    @PostMapping({"/upload"})
    @ApiOperation("上传文件，并返回url")
    public R<List<Map>> fileupload(@RequestPart("file")MultipartFile file) {
        List<Map> uploading = imgService.uploading(file);
        return R.data(uploading);
    }
}