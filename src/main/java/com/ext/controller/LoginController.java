package com.ext.controller;


import com.ext.commons.utils.R;
import com.ext.entity.ResponseResult;
import com.ext.entity.User;
import com.ext.service.LoginServcie;
import com.ext.vo.LoginVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(tags = "登录管理")
public class LoginController {

    @Autowired
    private LoginServcie loginServcie;

    @PostMapping("/user/login")
    @ApiOperation("用户登录")
    public R login(@RequestBody LoginVO user) {
        //登录
        User userInner = new User();
        userInner.setPassword(user.getPassword());
        userInner.setUsername(user.getUsername());
        return loginServcie.login(userInner);
    }

    @PostMapping("/user/logout")
    @ApiOperation("用户退出")
    public R<String> logout() {
        System.out.println("hello");
        return R.data(loginServcie.logout());
    }
}
