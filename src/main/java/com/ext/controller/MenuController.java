package com.ext.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ext.commons.utils.R;
import com.ext.entity.Menu;
import com.ext.service.IMenuService;
import com.ext.dto.AddMenuDTO;
import com.ext.vo.MenuPageVO;
import com.ext.vo.MenuVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/menu")
@Api(tags = "权限管理")
public class MenuController {

    @Autowired
    private IMenuService menuService;


    @GetMapping("/menuCache")
    @ApiOperation("权限信息带缓存")
    @PreAuthorize("hasAuthority('api:menu:menuCache')")
    public R<List> getDeptInfoByCache() {
        return R.data(menuService.getDeptInfoByCache());
    }

    @ApiOperation("获取权限信息列表")
    @GetMapping("/list")
    @PreAuthorize("hasAuthority('api:menu:list')")
    public R<List<MenuVO>> findMenu() {
        List<MenuVO> dept = menuService.findMenu();
        return R.data(dept);
    }


    //分页并模糊查询
    @ApiOperation("分页模糊查询信息")
    @PostMapping("/findData")
    @PreAuthorize("hasAuthority('api:menu:findData')")
    public R getMenuList(@RequestBody(required = false) MenuPageVO menuPageVO){
        Page list = menuService.getMenuist(menuPageVO);
        return R.data(list);
    }



    @PostMapping("/update")
    @ApiOperation("修改权限")
    @PreAuthorize("hasAuthority('api:menu:update')")
    public R<String> update(@RequestBody Menu menu) {
        return R.data(menuService.updateMenu(menu));
    }


    @GetMapping("/remove")
    @ApiOperation("根据id删除权限信息")
    @PreAuthorize("hasAuthority('api:menu:remove')")
    public R<String> remove(@RequestParam("id") Integer id) {
        return R.data(menuService.deletAll(id));
    }


    @ApiOperation("保存权限")
    @PostMapping("/save")
    @PreAuthorize("hasAnyAuthority('api:menu:save')")
    public R<String> addPermission(@RequestBody AddMenuDTO menuDTO){
        return  R.success(menuService.addPermission(menuDTO));
    }
}
