package com.ext.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.ext.commons.utils.R;
import com.ext.dto.GrantPermissionDTO;
import com.ext.entity.Role;
import com.ext.mapper.RoleMapper;
import com.ext.service.RoleService;
import com.ext.vo.AddRoleVO;
import com.ext.vo.RolePageVO;
import com.ext.vo.UpRoleVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/role")
@Slf4j
@Api(tags = "角色管理")
public class RoleController {

    @Autowired
    private RoleService roleService;
    @Autowired
    private RoleMapper roleMapper;

    @ApiOperation("分页模糊查询角色信息")
    @PostMapping("/findData")
    @PreAuthorize("hasAuthority('api:role:findData')")
    public R getRoleList(@RequestBody(required = false) RolePageVO rolePageVO){
        IPage<Role> list = roleService.getRoleList(rolePageVO);
        return  R.data(list);
    }


    @ApiOperation("获取角色信息列表")
    @GetMapping("/list")
    @PreAuthorize("hasAuthority('api:role:list')")
    public R<List> list() {
        return R.data(roleService.findAllRole());
    }

    @PostMapping("/save")
    @ApiOperation("保存角色信息")
    @PreAuthorize("hasAuthority('api:role:save')")
    public R<String> addRole(@RequestBody AddRoleVO roleVO) {
        return R.data(roleService.saveRole(roleVO));
    }


    @PostMapping("/update")
    @ApiOperation("修改角色信息")
    @PreAuthorize("hasAuthority('api:role:update')")
    public R<String> updateDept(@RequestBody UpRoleVO roleVO) {
        return R.data(roleService.upRole(roleVO));
    }

    @GetMapping("/remove")
    @ApiOperation("根据id删除角色信息")
    @PreAuthorize("hasAuthority('api:role:remove')")
    public R<String> remove(@RequestParam Integer id) {
        return R.data(roleService.deleteRoleById(id));

    }

    @PostMapping("/setPermission")
    @ApiOperation("给角色设置权限(批量设置)")
    public R<String> setPermission(@Validated @RequestBody GrantPermissionDTO grantPermissionDTO){
        return R.data(roleService.setPermission(grantPermissionDTO));

    }



}
