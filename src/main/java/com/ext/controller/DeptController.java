package com.ext.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.ext.commons.utils.R;
import com.ext.entity.Dept;
import com.ext.entity.KyDyssHistory;
import com.ext.service.DeptService;
import com.ext.vo.AddDeptVO;
import com.ext.vo.DeptPageVO;
import com.ext.vo.UpDeptVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


@RestController
@Api(tags="部门管理")
@RequestMapping("/dept")
public class DeptController  {

    @Autowired
    private DeptService deptService;


    @ApiOperation("获取部门列表信息")
    @GetMapping("/list")
    @PreAuthorize("hasAuthority('api:dept:list')")
    public R<String> list() {
        String dept = deptService.findDept();
        return R.data(dept);

    }


    @GetMapping("/deptCache")
    @ApiOperation("获取全部部门信息带缓存")
    @PreAuthorize("hasAuthority('api:dept:deptCache')")
    public R<List> getDeptInfoByCache() {
        return R.data(deptService.getDeptInfoByCache());
    }


    //分页并模糊查询
    @ApiOperation("分页模糊查询部门信息")
    @PostMapping("/findData")
    @PreAuthorize("hasAuthority('api:dept:findData')")
    @ResponseBody
    public R getDeptList(@RequestBody(required = false)  DeptPageVO deptPageVO){
        IPage<Dept> list = deptService.getDeptList(deptPageVO);
        return  R.data(list);
    }


    @PostMapping("/save")
    @ApiOperation("新增部门数据接口")
    @PreAuthorize("hasAuthority('api:dept:save')")
    public R<String> addDept(@RequestBody @Validated AddDeptVO deptVO){
        return R.data(deptService.saveDept(deptVO));
    }


    @PostMapping("/update")
    @ApiOperation("修改部门信息")
    @PreAuthorize("hasAuthority('api:dept:update')")
    public R<String> update(@RequestBody @Validated UpDeptVO deptVO){
        return  R.data(deptService.updateDept(deptVO));
    }



    @GetMapping("/remove")
    @PreAuthorize("hasAuthority('api:dept:remove')")
    @ApiOperation("根据id删除部门信息")
    public R<String> remove(@RequestParam("id") Integer id) {
        return R.data(deptService.deleteDeptById(id));
    }

    @ApiOperation("插入信息")
    @GetMapping("/chalist")
//    @PreAuthorize("hasAuthority('api:dept:list')")
    public R listDept() {
//        deptService.insertDept();
        List<KyDyssHistory> deptVo = deptService.findDeptVo();
        return R.data(deptVo);

    }
}
