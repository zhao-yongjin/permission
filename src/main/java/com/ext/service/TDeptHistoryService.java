package com.ext.service;

import com.ext.entity.TDeptHistory;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 我的公众号：MarkerHub
 * @since 2022-12-07
 */
public interface TDeptHistoryService extends IService<TDeptHistory> {

    List<TDeptHistory> findAll();


}
