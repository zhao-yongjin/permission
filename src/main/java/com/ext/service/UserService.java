package com.ext.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ext.commons.enums.ResultCode;
import com.ext.dto.RoleDTO;
import com.ext.entity.User;
import com.ext.vo.AddUserVO;
import com.ext.vo.LoginVO;
import com.ext.vo.UpUserVO;
import com.ext.vo.UserPageVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserService extends IService<User> {
//
//    String upImg(User user);

    List findAll();

    String inseterUser(AddUserVO userVO);

    String updateUser(UpUserVO userVO);

    String deleteById(Integer id);

    String updatePwd(LoginVO userVO);



    User getByUsername(String username);

    Page getUserList(UserPageVO userPageVO);

    String setRolelist(@Param("roleDTO") RoleDTO roleDTO);
}


