package com.ext.service;


import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

public interface ImgService {
    List<Map> uploading(MultipartFile file);
}
