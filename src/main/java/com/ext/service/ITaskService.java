package com.ext.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ext.entity.Task;

/**
 * <p>
 *  服务类
 * </p>
 */
public interface ITaskService extends IService<Task> {

}
