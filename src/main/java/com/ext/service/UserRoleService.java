package com.ext.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ext.entity.UserRole;

import java.util.List;

public interface UserRoleService extends IService<UserRole> {
    List<UserRole> findByRoleId(Integer rId);

    Integer deleteByRoleId(Integer rId);

}
