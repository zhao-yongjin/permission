package com.ext.service;

import com.ext.commons.utils.R;
import com.ext.entity.ResponseResult;
import com.ext.entity.User;

public interface LoginServcie {
    R login(User user);

    String logout();
}
