package com.ext.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ext.commons.enums.ResultCode;
import com.ext.entity.Menu;
import com.ext.dto.AddMenuDTO;
import com.ext.vo.MenuPageVO;
import com.ext.vo.MenuVO;

import java.util.List;

public interface IMenuService extends IService<Menu> {
    String deletAll(Integer id);

    List getDeptInfoByCache();

    String updateMenu(Menu menu);

    List<MenuVO> findMenu();

    Page getMenuist(MenuPageVO menuPageVO);

    String addPermission(AddMenuDTO menuDTO);
}