package com.ext.service;

import com.ext.entity.KyDyssHistory;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 地音监测系统实时数据 服务类
 * </p>
 *
 * @author 我的公众号：MarkerHub
 * @since 2022-12-07
 */
public interface KyDyssHistoryService extends IService<KyDyssHistory> {

    List<KyDyssHistory> findAll();
}
