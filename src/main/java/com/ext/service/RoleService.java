package com.ext.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ext.commons.enums.ResultCode;
import com.ext.dto.GrantPermissionDTO;
import com.ext.entity.Dept;
import com.ext.entity.Role;
import com.ext.vo.AddRoleVO;
import com.ext.vo.RolePageVO;
import com.ext.vo.UpRoleVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RoleService extends IService<Role> {


    String deleteRoleById(Integer roleId);

    List findAllRole();

    String saveRole(AddRoleVO roleVO);

    String upRole(UpRoleVO roleVO);

    String setPermission(@Param("grantPermissionDTO") GrantPermissionDTO grantPermissionDTO);

    IPage<Role> getRoleList(RolePageVO rolePageVO);
}
