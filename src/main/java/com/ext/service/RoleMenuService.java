package com.ext.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ext.entity.RoleMenu;

public interface RoleMenuService extends IService<RoleMenu> {
}
