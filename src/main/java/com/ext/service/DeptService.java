package com.ext.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ext.entity.Dept;
import com.ext.entity.KyDyssHistory;
import com.ext.vo.AddDeptVO;
import com.ext.vo.DeptPageVO;
import com.ext.vo.UpDeptVO;

import java.util.List;


public interface DeptService extends IService<Dept> {
    String deleteDeptById(Integer deptId);


    List getDeptInfoByCache();

    String updateDept(UpDeptVO dept);

    String findDept();

    String saveDept(AddDeptVO deptVO);


    IPage<Dept> getDeptList(DeptPageVO deptPageVO);

    void insertDept();

    List<KyDyssHistory> findDeptVo();
}
