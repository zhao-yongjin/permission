package com.ext.service.Impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ext.commons.enums.ResultCode;
import com.ext.commons.exception.HerinRuntimeException;
import com.ext.commons.utils.ObjectUtil;
import com.ext.commons.utils.RedisCache;
import com.ext.dto.GrantPermissionDTO;
import com.ext.entity.Role;
import com.ext.entity.RoleMenu;
import com.ext.mapper.RoleMapper;
import com.ext.mapper.RoleMenuMapper;
import com.ext.service.IMenuService;
import com.ext.service.RoleMenuService;
import com.ext.service.RoleService;
import com.ext.service.UserRoleService;
import com.ext.vo.AddRoleVO;
import com.ext.vo.RolePageVO;
import com.ext.vo.RoleVO;
import com.ext.vo.UpRoleVO;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static java.lang.String.format;

@Service
@Slf4j
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

    @Autowired
    private RedisCache redisCache;
    @Autowired
    private UserRoleService userRoleService;
    @Autowired
    private RoleMapper roleMapper;
    @Autowired
    private RoleMenuService roleMenuService;
    @Autowired
    private RoleMenuMapper roleMenuMapper;
    @Autowired
    private IMenuService menuService;


    @Override
    public String deleteRoleById(Integer roleId) {
        if (ObjectUtil.isEmpty(roleId)) {
            throw new HerinRuntimeException("");
        }
        LambdaQueryWrapper<Role> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(Role::getId, roleId);
        baseMapper.delete(queryWrapper);
        userRoleService.deleteByRoleId(roleId);
        return String.valueOf(ResultCode.SUCCESS);
    }

    @Override
    public List findAllRole() {
        List result;
        List<Role> roles = redisCache.getCacheList("Role");
        if (roles.isEmpty()) {
            LambdaQueryWrapper<Role> wrapper = Wrappers.lambdaQuery();
            List<Role> roles1 = roleMapper.selectList(wrapper);
            ArrayList<RoleVO> roleVOS = new ArrayList<>();
            for (Role role : roles1) {
                RoleVO roleVO = new RoleVO();
                BeanUtils.copyProperties(role, roleVO);
                roleVOS.add(roleVO);

            }
            redisCache.setCacheList("Role", roleVOS);
            result=roleVOS;
        }else {
            result=roles;
        }
        return  result;

    }

    @Override
    public String saveRole(AddRoleVO roleVO) {
        String rname = roleVO.getRname();
        if (StringUtils.isEmpty(rname)) {
            throw new HerinRuntimeException("");
        }
        Role role = BeanUtil.copyProperties(roleVO, Role.class);
        LambdaQueryWrapper<Role> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(Role::getRname, role.getRname());
        Role role1 = roleMapper.selectOne(queryWrapper);
        if (role1 != null) {
            throw new HerinRuntimeException("");
        } else {
            int flag = roleMapper.insert(role);
            if (flag == 1) {
                redisCache.deleteObject("Role");
                return String.valueOf(ResultCode.SUCCESS);
            }
            throw new HerinRuntimeException("");
        }

    }

    @Override
    public String setPermission(GrantPermissionDTO grantPermissionDTO) {
        // 获取角色id
        Integer roleId = grantPermissionDTO.getRoleId();
        // 获取授权集合
        List<Integer> permissionIdList = grantPermissionDTO.getPermissionIdList();
        // 执行批量插入
        if (permissionIdList.isEmpty() || ObjectUtil.isEmpty(roleId)) {
            throw new HerinRuntimeException("");
        }
        // 如果传入的roleId不等于库里的roleId
        LambdaQueryWrapper<Role> roleWrapper = Wrappers.lambdaQuery();
        roleWrapper.eq(Role::getId, roleId);
        Role role = roleMapper.selectOne(roleWrapper);
        if (ObjectUtil.isEmpty(role)) {
            return String.valueOf(ResultCode.NOT_EXIST);
        }
        // 获取当前角色已经存在哪些权限
        LambdaQueryWrapper<RoleMenu> roleMenuWrapper = Wrappers.lambdaQuery();
        roleMenuWrapper.eq(RoleMenu::getRid, roleId);
        List<RoleMenu> existsRoleMenuList = roleMenuMapper.selectList(roleMenuWrapper);
        // 如果当前角色已经存在权限
        if (!existsRoleMenuList.isEmpty()) {
            // 定义当前角色已经有的菜单列表
            Set<Integer> existsMenuSet = existsRoleMenuList.stream().map(RoleMenu::getMid).collect(Collectors.toSet());
            // 如果当前角色已经存在以下权限
            log.info("roleId:{}已经已经有权限如下:{}", roleId, existsMenuSet);
            // 根据两个权限取差集
            Sets.SetView<Integer> needAddMenuSet = Sets.difference(Sets.newHashSet(permissionIdList), existsMenuSet);
//            if (!needAddMenuSet.isEmpty()) {
//                // 如果当前需要插入权限集合不为空则插入
//                grantPermissionDTO.setPermissionIdList(Lists.newArrayList(needAddMenuSet));
//            } else {
//                grantPermissionDTO.setPermissionIdList(Lists.newArrayList());
//            }
            grantPermissionDTO.setPermissionIdList(Lists.newArrayList(needAddMenuSet));
        }
        List<Integer> finalPermissionIdList = grantPermissionDTO.getPermissionIdList();
        // 最终需要新增的权限集合如果不为空则执行插入
        if (!finalPermissionIdList.isEmpty()) {
            Integer batchInsertRoleMenuResult = roleMenuMapper.batchInsertRoleMenu(grantPermissionDTO);
            if (ObjectUtil.isEmpty(batchInsertRoleMenuResult)) {
                redisCache.deleteObject("Role");
            }
        }
        return format("插入成功权限个数:%s", finalPermissionIdList.size());

    }

    @Override
    public IPage<Role> getRoleList(RolePageVO rolePageVO) {
        String rname = rolePageVO.getRname();
        QueryWrapper<Role> wrapper = new QueryWrapper<>();
        if (Strings.isNotBlank(rname)){
            wrapper.lambda().like(Role::getRname,rname);
        }
        int currentPage = rolePageVO.getCurrentPage();
        int pageSize = rolePageVO.getPageSize();
        Page<Role> page = new Page<>();
        if(ObjectUtil.isEmpty(currentPage) || ObjectUtil.isEmpty(pageSize))
        {
            Page<Role> rolePage = this.page(page, wrapper);
            return rolePage;
        }
        page.setCurrent(currentPage);
        page.setSize(pageSize);
        Page<Role> rolePages = this.page(page, wrapper);
        return rolePages;
    }


    @Override
    public String upRole(UpRoleVO roleVO) {
        Integer roleId = roleVO.getId();
        String rname = roleVO.getRname();
        if (ObjectUtil.isEmpty(roleId) || StringUtils.isEmpty(rname)) {
            throw new HerinRuntimeException("");
        }
        Role role = BeanUtil.copyProperties(roleVO, Role.class);
        LambdaQueryWrapper<Role> query = Wrappers.lambdaQuery();
        query.eq(Role::getRname, role.getRname());
        Role role1 = baseMapper.selectOne(query);
        if (role1 == null) {
            LambdaQueryWrapper<Role> queryWrapper = Wrappers.lambdaQuery();
            queryWrapper.eq(Role::getId, role.getId());
            int flag = roleMapper.update(role, queryWrapper);
            if (flag == 1) {
                redisCache.deleteObject("Role");
                return String.valueOf(ResultCode.SUCCESS);
            }
            throw new HerinRuntimeException("");
        }
        if (role1 != null) {
            if (role1.getId().equals(role.getId())) {
                LambdaQueryWrapper<Role> queryWrapper = Wrappers.lambdaQuery();
                queryWrapper.eq(Role::getId, role.getId());
                int flag = roleMapper.update(role, queryWrapper);
                if (flag == 1) {
                    redisCache.deleteObject("Role");
                    return String.valueOf(ResultCode.SUCCESS);
                }
                throw new HerinRuntimeException("");
            }
            throw new HerinRuntimeException("");
        }
        throw new HerinRuntimeException("");
    }
}
