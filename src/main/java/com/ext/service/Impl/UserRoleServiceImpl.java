package com.ext.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ext.entity.UserRole;
import com.ext.mapper.UserRoleMapper;
import com.ext.service.UserRoleService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserRoleServiceImpl  extends ServiceImpl<UserRoleMapper, UserRole> implements UserRoleService {

    @Override
    public List<UserRole> findByRoleId(Integer rId) {
        LambdaQueryWrapper<UserRole> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(UserRole::getRid, rId);
        List<UserRole> userRoles = baseMapper.selectList(queryWrapper);
        return userRoles;
    }
    @Override
    public Integer deleteByRoleId(Integer rId) {
        LambdaQueryWrapper<UserRole> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(UserRole::getRid, rId);
        int delete = baseMapper.delete(queryWrapper);
        return delete;
    }
}
