package com.ext.service.Impl;


import com.ext.commons.enums.ResultCode;
import com.ext.commons.exception.HerinRuntimeException;
import com.ext.commons.utils.JwtUtil;
import com.ext.commons.utils.R;
import com.ext.commons.utils.RedisCache;
import com.ext.entity.LoginUser;
import com.ext.entity.User;
import com.ext.service.LoginServcie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


@Service
public class LoginServiceImpl implements LoginServcie {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private RedisCache redisCache;

    @Override
    public R login(User user) {
        //AuthenticationManager authenticate进行用户认证
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(user.getUsername(),user.getPassword());
        Authentication authenticate = authenticationManager.authenticate(authenticationToken);
        //如果认证没通过，给出对应的提示
        if(Objects.isNull(authenticate)){
            throw new HerinRuntimeException("用户名密码错误");
        }
        //如果认证通过了，使用userid生成一个jwt jwt存入ResponseResult返回
        LoginUser loginUser = (LoginUser) authenticate.getPrincipal();
        String userid = loginUser.getUser().getId().toString();
        String jwt = JwtUtil.createJWT(userid);
        Map<String,String> map = new HashMap<>();
        map.put("userId",userid);
        map.put("username",user.getUsername());
        map.put("token",jwt);
        //把完整的用户信息存入redis  userid作为key
        redisCache.setCacheObject("login",loginUser);
        return  R.data(200,map,"登录成功");
    }

    @Override
    public String logout() {
        //获取SecurityContextHolder中的用户id
        SecurityContext context = SecurityContextHolder.getContext();
        UsernamePasswordAuthenticationToken authentication = (UsernamePasswordAuthenticationToken) context.getAuthentication();
        LoginUser loginUser = (LoginUser) authentication.getPrincipal();
        Long userId = loginUser.getUser().getId();
        System.out.println("userId"+userId);
        authentication.setAuthenticated(false);
        //删除redis中的值
        redisCache.deleteObject("login:");
        context.setAuthentication(null);
        return String.valueOf(ResultCode.SUCCESS);
    }
}
