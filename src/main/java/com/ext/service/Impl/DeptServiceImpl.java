package com.ext.service.Impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ext.commons.enums.ResultCode;
import com.ext.commons.exception.HerinRuntimeException;
import com.ext.commons.utils.RedisCache;
import com.ext.entity.Dept;
import com.ext.entity.KyDyssHistory;
import com.ext.mapper.DeptMapper;
import com.ext.mapper.KyDyssHistoryMapper;
import com.ext.mapper.UserMapper;
import com.ext.service.DeptService;
import com.ext.vo.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.Strings;
import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.Collator;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class DeptServiceImpl extends ServiceImpl<DeptMapper, Dept> implements DeptService {
    @Autowired
    private UserMapper userMapper;

    @Autowired
    private DeptMapper deptMapper;

    @Autowired
    private RedisCache redisCache;

   @Autowired
    private KyDyssHistoryMapper kyDyssHistoryMapper;


    @Override
    public IPage<Dept>  getDeptList(DeptPageVO deptPageVO) {
        String dname = deptPageVO.getDname();
        QueryWrapper<Dept> wrapper = new QueryWrapper<>();
        if (Strings.isNotBlank(dname)){
            wrapper.lambda().like(Dept::getDName,dname);
        }
        int currentPage = deptPageVO.getCurrentPage();
        int pageSize = deptPageVO.getPageSize();
        Page<Dept> page = new Page<>();
        if(ObjectUtil.isEmpty(currentPage) ||  ObjectUtil.isEmpty(pageSize))
        {
            Page<Dept> deptPage = this.page(page, wrapper);
            return deptPage;
        }

        page.setCurrent(currentPage);
        page.setSize(pageSize);
        Page<Dept> deptPages = this.page(page, wrapper);
        List<Dept> records = deptPages.getRecords();
        //按姓名排序
        List<Dept> collect = records.stream().sorted((o1, o2) -> {
            Comparator<Object> compare = Collator.getInstance(Locale.CHINA);
            return compare.compare(o1.getDName(), o2.getDName());
        }).collect(Collectors.toList());
        deptPages.setRecords(collect);
        return deptPages;
    }



    @Override
    public String deleteDeptById(Integer deptId) {
        if(ObjectUtil.isEmpty(deptId)){
            throw new HerinRuntimeException("无效的账号");
        }
        // 根据部门id查询对应部门是否存在用户
        Integer result = userMapper.getUserCountByDeptId(deptId);
        if (ObjectUtil.isEmpty(result)) {
            // 如果部门下面有用户则不能删除
            throw new HerinRuntimeException("无效的账号");
        }
        redisCache.deleteObject("DeptList");
        int deleteResult = deptMapper.deleteById(deptId);
        if(deleteResult==1){
            return String.valueOf(ResultCode.SUCCESS);
        }
        throw new HerinRuntimeException("失败");
    }

    @Override
    public List getDeptInfoByCache() {
        List result;
        List<Dept> cachelist = redisCache.getCacheList("DeptList");
        if (cachelist.isEmpty()) {
            LambdaQueryWrapper<Dept> wrapper = Wrappers.lambdaQuery();
            List<Dept> depts = deptMapper.selectList(wrapper);
            ArrayList<DeptVO> deptsVos = new ArrayList<>();
            for (Dept dept : depts) {
                DeptVO deptVO = new DeptVO();
                BeanUtils.copyProperties(dept, deptVO);
                deptsVos.add(deptVO);
            }
            redisCache.setCacheList("DeptList", deptsVos);
            result = deptsVos;
        } else {
            result = cachelist;
        }
        return result;
    }
    @Override
    public String saveDept(AddDeptVO deptVO)throws ServiceException {
        String dname = deptVO.getDname();
        if (StringUtils.isEmpty(dname)) {
            throw new HerinRuntimeException("");
        }
        Dept dept = BeanUtil.copyProperties(deptVO,Dept.class);
        LambdaQueryWrapper<Dept> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(Dept::getDName, dept.getDName());
        Dept depts = deptMapper.selectOne(queryWrapper);
        if (depts != null) {
            throw new HerinRuntimeException("");
        } else {
            int flag = deptMapper.insert(dept);
            if (flag == 1) {
                redisCache.deleteObject("DeptList");
                return String.valueOf(ResultCode.SUCCESS);
            }
            throw new HerinRuntimeException("");
        }


    }



    @Override
    public String updateDept(UpDeptVO deptVO) {
        Integer deptId = deptVO.getId();
        String dname = deptVO.getDname();
        if (ObjectUtil.isEmpty(deptId)||StringUtils.isEmpty(dname)){
            throw new HerinRuntimeException("");
        }
        Dept dept = BeanUtil.copyProperties(deptVO, Dept.class);
        LambdaQueryWrapper<Dept> query = Wrappers.lambdaQuery();
        query.eq(Dept::getDName, dept.getDName());
        Dept dept1 = baseMapper.selectOne(query);
        if (dept1 == null) {
            LambdaQueryWrapper<Dept> queryWrapper = Wrappers.lambdaQuery();
            queryWrapper.eq(Dept::getId, dept.getId());
            int flag = deptMapper.update(dept,queryWrapper);
            if (flag == 1) {
                redisCache.deleteObject("DeptList");
                return String.valueOf(ResultCode.SUCCESS);
            }
            throw new HerinRuntimeException("");
        }
        if (dept1 != null) {
            if (dept1.getId().equals(dept.getId())) {
                LambdaQueryWrapper<Dept> queryWrapper = Wrappers.lambdaQuery();
                queryWrapper.eq(Dept::getId, dept.getId());
                int flag = deptMapper.update(dept, queryWrapper);
                if (flag == 1) {
                    redisCache.deleteObject("DeptList");
                    return String.valueOf(ResultCode.SUCCESS);
                }
                throw new HerinRuntimeException("");
            }
            throw new HerinRuntimeException("");
        }
        throw new HerinRuntimeException("");

    }


    @Override
    public String findDept() {
        LambdaQueryWrapper<Dept> wrapper = Wrappers.lambdaQuery();
        String integer = baseMapper.selectCount(wrapper).toString();
        log.info(String.valueOf(integer));
//        QueryWrapper<Dept> wrapper = new QueryWrapper<>();
//        List<Dept> depts = deptMapper.selectList(wrapper);
//        wrapper.like(Dept::getDname,"a").lt(Dept::getSalary,7000).notLike(Dept::getDname,"xs");
//        List<Dept> depts = baseMapper.selectList(wrapper);
//        List<Dept> collect = depts.stream().sorted(Comparator.comparing(Dept::getCreateTime)).collect(Collectors.toList());
////        String s = JSONObject.toJSONString(collect);
//       // long test = redisCache.setCacheList("test",collect);
//        List<Object> test1 = redisCache.getCacheList("test");
////        //获取近3小时数据
//        Calendar calendar = Calendar.getInstance();
////        calendar.add(Calendar.HOUR, -3);
//        calendar.add(Calendar.HOUR,-24);
//        queryWrapper.gt(Dept::getCreateTime, calendar.getTime());
//        List<PersonNumListVo> personNumListVos = deptMapper.selectPersonCount();
//        for (PersonNumListVo personNumListVo : personNumListVos) {
//            personNumListVo.setS("aaa");
//        }
//                .filter(m->!m.getDname().equals("java"))
//                .collect(Collectors.groupingBy(m->m.getCreateTime().equals(""));
//                .sorted(Comparator.comparing(Dept::getCreateTime)).collect(Collectors.toList());
//        List<Dept> cachelist = redisCache.getCacheList("DeptList");
//        if(ObjectUtil.isNotEmpty(cachelist)){
//            return cachelist;
//        }
//        redisCache.setCacheList("DeptList", collect);
//        List<String> collect = depts.stream()
//                .map(e -> e.getDname())
//                .collect(Collectors.toList());
        return integer;
    }
    @Override
    public void insertDept() {
//        List<Dept> list =new ArrayList<>();
//            Dept dept = new Dept();
//            dept.setDName("aa");
//            dept.setSalary(4000);
//            dept.setRemark("ab");
//            dept.setCreateName("cc");
//            deptMapper.insert(dept);


    }

    @Override
    public List<KyDyssHistory> findDeptVo() {
        LambdaQueryWrapper<KyDyssHistory> query = Wrappers.lambdaQuery();
        List<KyDyssHistory> kyDyssHistories = kyDyssHistoryMapper.selectList(query);
//        List<String> collect = kyDyssHistories.stream().map(KyDyssHistory::getSensorLoc).collect(Collectors.toList());
//        List<String> collect1 = kyDyssHistories.stream().map(KyDyssHistory::getValidEventEnergy).collect(Collectors.toList());
//        collect.add(0,"aabbcc");
//        HashMap map = new HashMap();
//        map.put("SensorLoc",collect);
//        map.put("validEventEnergy",collect1);
        redisCache.setCacheList("aa:deptvo",kyDyssHistories);
        return  kyDyssHistories;
    }

}
