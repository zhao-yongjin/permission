package com.ext.service.Impl;

import com.ext.entity.KyDyss;
import com.ext.mapper.KyDyssMapper;
import com.ext.service.KyDyssService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 地音监测系统实时数据 服务实现类
 * </p>
 *
 * @author 我的公众号：MarkerHub
 * @since 2022-12-07
 */
@Service
public class KyDyssServiceImpl extends ServiceImpl<KyDyssMapper, KyDyss> implements KyDyssService {

}
