package com.ext.service.Impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ext.entity.Task;
import com.ext.mapper.TaskMapper;
import com.ext.service.ITaskService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 */
@Service
public class TaskServiceImpl extends ServiceImpl<TaskMapper, Task> implements ITaskService {

}
