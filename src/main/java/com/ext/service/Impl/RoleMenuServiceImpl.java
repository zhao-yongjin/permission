package com.ext.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ext.entity.RoleMenu;
import com.ext.mapper.RoleMenuMapper;
import com.ext.service.RoleMenuService;
import org.springframework.stereotype.Service;

@Service
public class RoleMenuServiceImpl extends ServiceImpl<RoleMenuMapper, RoleMenu> implements RoleMenuService {
}
