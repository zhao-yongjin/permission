package com.ext.service.Impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ext.commons.enums.ResultCode;
import com.ext.commons.exception.HerinRuntimeException;
import com.ext.commons.utils.RedisCache;
import com.ext.dto.RoleDTO;
import com.ext.entity.*;
import com.ext.mapper.UserMapper;
import com.ext.mapper.UserRoleMapper;
import com.ext.service.UserRoleService;
import com.ext.service.UserService;
import com.ext.vo.*;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import static java.lang.String.format;


@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
    private static final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);
    @Autowired
    private RedisCache redisCache;
    @Autowired
    private UserRoleMapper userRoleMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserRoleService userRoleService;
    @Autowired
    BCryptPasswordEncoder passwordEncoder;


    @Override
    public String deleteById(Integer id) {
        User user = baseMapper.selectById(id);
        if (ObjectUtil.isEmpty(user)) {
            throw new HerinRuntimeException("");
        } else {
            baseMapper.deleteById(id);
            userRoleMapper.deleteByUserId(id);
            return String.valueOf(ResultCode.SUCCESS);
        }
    }

    @Override
    public List findAll() {
        List result;
        LoginUser loginUser = (LoginUser) redisCache.getCacheObject("login");
        log.info(loginUser.getUsername());
        List<UserVO> users = redisCache.getCacheList("User");
        if (users.isEmpty()) {
            LambdaQueryWrapper<User> wrapper = Wrappers.lambdaQuery();
            List<User> users1 = userMapper.selectList(wrapper);
            List<UserVO> userVOS = new ArrayList<>();
            for (User user : users1) {
                UserVO userVO = new UserVO();
                BeanUtils.copyProperties(user, userVO);
                userVOS.add(userVO);
            }
            redisCache.setCacheList("User", userVOS);
            if (!loginUser.getUsername().equals("admin")) {
                List<UserVO> collect = userVOS.stream().filter(u -> !"admin".equals(u.getUsername())).collect(Collectors.toList());
                log.info("普通用户");
                result=collect;
            }
            log.info("admin用户");
            result=userVOS;
        }else {
        if (loginUser.getUsername().equals("admin")) {
            log.info("admin用户");
            result=users;
        } else {
            List<UserVO> collects = users.stream().filter(u -> !"admin".equals(u.getUsername())).collect(Collectors.toList());
            log.info("普通用户");
            result=collects;
        }
        }
        return  result;
    }


    @Override
    public String inseterUser(AddUserVO userVO) {
        User user = BeanUtil.copyProperties(userVO, User.class);
        log.info(user.toString());
        String username = userVO.getUsername();
        if (StringUtils.isEmpty(username)) {
            throw new HerinRuntimeException("");
        }
        String password = user.getPassword();
        if (password == null || password.equals("")) {
            user.setPassword((new BCryptPasswordEncoder()).encode("123456"));
        }
        LambdaQueryWrapper<User> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(User::getUsername, user.getUsername());
        User user1 = baseMapper.selectOne(queryWrapper);
        if (user1 != null) {
            throw new HerinRuntimeException("");
        } else {
            int insert = baseMapper.insert(user);
            if (insert == 1) {
                this.redisCache.deleteObject("User");
                return String.valueOf(ResultCode.SUCCESS);
            } else {
                throw new HerinRuntimeException("");
            }
        }

    }

    @Override
    public String updateUser(UpUserVO userVO) {
        Long userId = userVO.getId();
        String username = userVO.getUsername();
        if (ObjectUtil.isEmpty(userId) || StringUtils.isEmpty(username)) {
            throw new HerinRuntimeException("");
        }
        User user = BeanUtil.copyProperties(userVO, User.class);
        LambdaQueryWrapper<User> query = Wrappers.lambdaQuery();
        query.eq(User::getUsername, user.getUsername());
        User user1 = baseMapper.selectOne(query);
        if (user1 == null) {
            LambdaQueryWrapper<User> queryWrapper = Wrappers.lambdaQuery();
            queryWrapper.eq(User::getId, user.getId());
            int flag = userMapper.update(user, queryWrapper);
            if (flag == 1) {
                redisCache.deleteObject("User");
                return String.valueOf(ResultCode.SUCCESS);
            }
            throw new HerinRuntimeException("");
        }
        if (user1 != null) {
            if (user1.getId().equals(user.getId())) {
                LambdaQueryWrapper<User> queryWrapper = Wrappers.lambdaQuery();
                queryWrapper.eq(User::getId, user.getId());
                int flag = userMapper.update(user, queryWrapper);
                if (flag == 1) {
                    redisCache.deleteObject("User");
                    return String.valueOf(ResultCode.SUCCESS);
                }
                throw new HerinRuntimeException("");
            }


        }
        throw new HerinRuntimeException("");
    }

    @Override
    public String updatePwd(LoginVO userVO) {
        User user = BeanUtil.copyProperties(userVO, User.class);
        String pwd = (new BCryptPasswordEncoder()).encode("123456");
        user.setPassword(pwd);
        LambdaQueryWrapper<User> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(User::getUsername, user.getUsername());
        baseMapper.update(user, queryWrapper);
        redisCache.deleteObject("User");
        return String.valueOf(ResultCode.SUCCESS);
    }

    @Override
    public String setRolelist(RoleDTO roleDTO) {
        // 获取角色id
        Integer userId = roleDTO.getUserId();
        // 获取授权集合
        List<Integer> roleIdList = roleDTO.getRoleIdList();
        // 执行批量插入
        if (roleIdList.isEmpty() || ObjectUtil.isEmpty(userId)) {
            throw new HerinRuntimeException("");
        }
        // 如果传入的roleId不等于库里的roleId
        LambdaQueryWrapper<User> userWrapper = Wrappers.lambdaQuery();
        userWrapper.eq(User::getId, userId);
        User user = userMapper.selectOne(userWrapper);
        if (user == null) {
            return String.valueOf(ResultCode.NOT_EXIST);
        }
        // 获取当前角色已经存在哪些权限
        LambdaQueryWrapper<UserRole> userRoleWrapper = Wrappers.lambdaQuery();
        userRoleWrapper.eq(UserRole::getUid, userId);
        List<UserRole> existsUserRoleList = userRoleMapper.selectList(userRoleWrapper);
        // 如果当前角色已经存在权限
        if (!existsUserRoleList.isEmpty()) {
            // 定义当前角色已经有的菜单列表
            Set<Integer> existsRoleSet = existsUserRoleList.stream().map(UserRole::getRid).collect(Collectors.toSet());
            // 如果当前角色已经存在以下权限
            log.info("userId:{}已经有角色如下:{}", userId, existsRoleSet);
            // 根据两个权限取差集
            Sets.SetView<Integer> needAddRoleSet = Sets.difference(Sets.newHashSet(roleIdList), existsRoleSet);
            roleDTO.setRoleIdList(Lists.newArrayList(needAddRoleSet));
        }
        List<Integer> finalRoleIdList = roleDTO.getRoleIdList();
        // 最终需要新增的权限集合如果不为空则执行插入
        if (!finalRoleIdList.isEmpty()) {
            Integer i = userRoleMapper.batchInsertUserRole(roleDTO);
        }
        return format("插入成功角色个数:%s", finalRoleIdList.size());

    }

    @Override
    public User getByUsername(String username) {
        return getOne(new QueryWrapper<User>().eq("username", username));
    }

    @Override
    public Page getUserList(UserPageVO userPageVO) {
        String username = userPageVO.getUsername();
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        if (Strings.isNotBlank(userPageVO.getUsername())) {
            wrapper.lambda().like(User::getUsername, username);
        }
        Page page = new Page<>();
        int currentPage = userPageVO.getCurrentPage();
        int pageSize = userPageVO.getPageSize();
        if (ObjectUtil.isEmpty(currentPage) && ObjectUtil.isEmpty(pageSize)){
            Page userPage = baseMapper.selectPage(page, wrapper);
            List<User> records = userPage.getRecords();
            List<UserVO> userVOS = new ArrayList<>();
            for (User record : records) {
                UserVO userVO = new UserVO();
                BeanUtils.copyProperties(record, userVO);
                userVOS.add(userVO);
            }
            userPage.setRecords(userVOS);
            return userPage;
        }else {
            page.setCurrent(userPageVO.getCurrentPage());
            page.setSize(userPageVO.getPageSize());
            Page userPage1 = baseMapper.selectPage(page, wrapper);
            List<User> records = userPage1.getRecords();
            List<UserVO> userVOS = new ArrayList<>();
            for (User record : records) {
                UserVO userVO = new UserVO();
                BeanUtils.copyProperties(record, userVO);
                userVOS.add(userVO);
            }
            userPage1.setRecords(userVOS);
            return userPage1;
        }
    }


}
