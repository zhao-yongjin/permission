package com.ext.service.Impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.ext.entity.Dept;
import com.ext.entity.TDeptHistory;
import com.ext.mapper.DeptMapper;
import com.ext.mapper.TDeptHistoryMapper;
import com.ext.service.DeptService;
import com.ext.service.TDeptHistoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 我的公众号：MarkerHub
 * @since 2022-12-07
 */
@Service
public class TDeptHistoryServiceImpl extends ServiceImpl<TDeptHistoryMapper, TDeptHistory> implements TDeptHistoryService {
    @Autowired
    private  DeptService deptService;

    @Autowired
    private DeptMapper deptMapper;
    @Autowired
    private  TDeptHistoryMapper deptHistoryMapper;
    @Override
    public List<TDeptHistory> findAll() {
        List<Dept> list = deptService.list();
        List<TDeptHistory> deptHistoryList = new ArrayList<>();
        for (Dept dept : list) {
            TDeptHistory stressHistory = new TDeptHistory();
//            stressHistory.setId(UUID.randomUUID());
            stressHistory.setDname(dept.getDName());
            stressHistory.setRemark(dept.getRemark());
            stressHistory.setSalary(dept.getSalary());
            stressHistory.setCreateName(dept.getCreateName());
            deptHistoryList.add(stressHistory);
        }
        this.saveBatch(deptHistoryList);
        return deptHistoryList;
    }


}
