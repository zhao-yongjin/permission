package com.ext.service.Impl;

import com.ext.entity.User;

import com.ext.service.ImgService;
import com.ext.service.UserService;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class ImgServiceImpl implements ImgService {
    SimpleDateFormat sdf = new SimpleDateFormat("/yyyy.MM.dd/");
    @Value("${file-save-path}")
    private String fileSavePath;
    @Autowired
    private UserService userService;
    public List<Map> uploading(MultipartFile file) {
        List<Map> result = new ArrayList();
        Map<String, Object> data = new HashMap();
        String originName = file.getOriginalFilename();
        System.out.println("originName:" + originName);
        if (!originName.endsWith(".jpg")) {
            data.put("status", "error");
            data.put("msg", "文件类型不对");
            result.add(data);
            return result;
        } else {
//            String format = this.sdf.format(new Date());
            String realPath = this.fileSavePath ;
            System.out.println("realPath:" + realPath);
            File folder = new File(realPath);
            if (!folder.exists()) {
                folder.mkdirs();
            }

            String newName = UUID.randomUUID().toString() + ".jpg";

            try {
                File file1 = new File(realPath + newName);
//                file.transferTo(file1);
                FileUtils.copyInputStreamToFile(file.getInputStream(), file1);
                //获得本机Ip（获取的是服务器的Ip）
//                InetAddress inetAddress = InetAddress.getLocalHost();
//                String ip = inetAddress.getHostAddress();
//                String replace = fileSavePath.replace("/usr/local/nginx/zyj", "192.168.216.129");
//                String url = replace + newName;
                String url =fileSavePath+newName;
                data.put("status","success");
                data.put("url",url);
//                User user = new User();
//                user.setUsername("gdssfe");
//                user.setImgPath(url);
//                userService.save(user);
//                userService.upImg(user);
            } catch (IOException var13) {
                data.put("status", "error");
                data.put("msg", var13.getMessage());
            }

            result.add(data);
            return result;
        }
    }
}
