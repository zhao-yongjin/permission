package com.ext.service.Impl;

import com.ext.entity.KyDyss;
import com.ext.entity.KyDyssHistory;
import com.ext.mapper.KyDyssHistoryMapper;
import com.ext.service.KyDyssHistoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ext.service.KyDyssService;
import com.google.common.collect.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 地音监测系统实时数据 服务实现类
 * </p>
 *
 * @author 我的公众号：MarkerHub
 * @since 2022-12-07
 */
@Service
public class KyDyssHistoryServiceImpl extends ServiceImpl<KyDyssHistoryMapper, KyDyssHistory> implements KyDyssHistoryService {
    @Autowired
    private KyDyssService kyDyssService;


    @Override
    public List<KyDyssHistory> findAll() {
        //实时
        List<KyDyss> list = kyDyssService.list();
        //历史
        List<KyDyssHistory> kylist = new ArrayList<>();


        // 根据两个权限取差集

            for (KyDyss kyDyss : list) {
                KyDyssHistory kyDyssHistory = new KyDyssHistory();
                kyDyssHistory.setEventCode(kyDyss.getEventCode());
                kyDyssHistory.setChannelNum(kyDyss.getChannelNum());
                kyDyssHistory.setSensorLoc(kyDyss.getSensorLoc());
                kyDyssHistory.setValidEventEnergy(kyDyss.getValidEventEnergy());
                kyDyssHistory.setValidEventTime(kyDyss.getValidEventTime());
                kylist.add(kyDyssHistory);
            }
            this.saveBatch(kylist);
            kylist.stream().distinct().collect(Collectors.toList());
            return kylist;
        }


}
