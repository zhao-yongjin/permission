package com.ext.service.Impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ext.commons.enums.ResultCode;
import com.ext.commons.exception.HerinRuntimeException;
import com.ext.commons.utils.ObjectUtil;
import com.ext.commons.utils.RedisCache;
import com.ext.entity.Menu;
import com.ext.mapper.MenuMapper;
import com.ext.mapper.RoleMenuMapper;
import com.ext.service.IMenuService;
import com.ext.dto.AddMenuDTO;
import com.ext.vo.MenuPageVO;
import com.ext.vo.MenuVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements IMenuService {
    @Autowired
    private MenuMapper menuMapper;

    @Autowired
    private RoleMenuMapper roleMenuMapper;

    @Autowired
    private RedisCache redisCache;



    @Override
    public String deletAll(Integer id) {

        if (ObjectUtil.isNotEmpty(id)) {
            throw new HerinRuntimeException("");
        }else {
        redisCache.deleteObject("MenuList");
        int i = baseMapper.deleteById(id);
        Integer integer = roleMenuMapper.deleteByMId(id);
        if(i==1 ){
            return String.valueOf(ResultCode.SUCCESS);
        }else {
            throw new HerinRuntimeException("");
        }
        }

    }

    @Override
    public List getDeptInfoByCache() {
        List result;
        List<Menu> cachelist = redisCache.getCacheList("MenuList");
        if (cachelist.isEmpty()) {
            LambdaQueryWrapper<Menu> wrapper = Wrappers.lambdaQuery();
            List<Menu> menus = menuMapper.selectList(wrapper);
            ArrayList<MenuVO> menuVOS = new ArrayList<>();
            for (Menu menu : menus) {
                MenuVO menuVO = new MenuVO();
                BeanUtils.copyProperties(menu, menuVO);
                menuVOS.add(menuVO);

            }
            redisCache.setCacheList("MenuList", menuVOS);
            result=menuVOS;
        }else {
            result=cachelist;
        }
        return  result;

    }


    @Override
    public String updateMenu(Menu menu) {
        Integer menuId = menu.getId();
        String permsName = menu.getPermsName();
        String perms = menu.getPerms();
        if (ObjectUtil.isEmpty(menuId) || StringUtils.isEmpty(permsName)|| StringUtils.isEmpty(perms)) {
            throw new HerinRuntimeException("");
        }
        LambdaQueryWrapper<Menu> query = Wrappers.lambdaQuery();
        query.eq(Menu::getPermsName, menu.getPermsName());
        Menu menu1 = baseMapper.selectOne(query);
        Integer menu1Id = menu1.getId();
        if (menu1 == null) {
            LambdaQueryWrapper<Menu> queryWrapper = Wrappers.lambdaQuery();
            queryWrapper.eq(Menu::getId, menu1Id);
            int flag = menuMapper.update(menu, queryWrapper);
            if (flag == 1) {
                redisCache.deleteObject("MenuList");
                return String.valueOf(ResultCode.SUCCESS);
            }
            throw new HerinRuntimeException("");
        }
        if (menu1 != null) {

            if (menu1Id.equals(menuId)) {
                LambdaQueryWrapper<Menu> queryWrapper = Wrappers.lambdaQuery();
                queryWrapper.eq(Menu::getId, menu.getId());
                int flag = menuMapper.update(menu, queryWrapper);
                if (flag == 1) {
                    redisCache.deleteObject("MenuList");
                    return String.valueOf(ResultCode.SUCCESS);
                }
                throw new HerinRuntimeException("");
            }
            throw new HerinRuntimeException("");
        }
        throw new HerinRuntimeException("");
    }


    @Override
    public List<MenuVO> findMenu() {
        LambdaQueryWrapper<Menu> queryWrapper = Wrappers.lambdaQuery();
        List<Menu> menus = menuMapper.selectList(queryWrapper);
        ArrayList<MenuVO> menuVos = new ArrayList<>();
        for (Menu menu : menus) {
            MenuVO menuVO = new MenuVO();
            BeanUtils.copyProperties(menu, menuVO);
            menuVos.add(menuVO);
        }
        return menuVos;
    }

    @Override
    public Page getMenuist(MenuPageVO menuPageVO) {
        String permsName = menuPageVO.getPermsName();
        QueryWrapper<Menu> wrapper = new QueryWrapper<>();
        if (Strings.isNotBlank(menuPageVO.getPermsName())){
            wrapper.lambda().like(Menu::getPermsName,permsName);
        }
        Page page = new Page<>();
        int currentPage = menuPageVO.getCurrentPage();
        int pageSize = menuPageVO.getPageSize();
        if(ObjectUtil.isEmpty(currentPage)&& ObjectUtil.isEmpty(pageSize))
        {
            Page menuPage = baseMapper.selectPage(page, wrapper);
            return menuPage;
        }
        page.setCurrent(menuPageVO.getCurrentPage());
        page.setSize(menuPageVO.getPageSize());
        Page menuPage1 = baseMapper.selectPage(page, wrapper);
        return menuPage1;
    }

    @Override
    public String addPermission(AddMenuDTO menuDTO) {
        String perms = menuDTO.getPerms();
        String permsName = menuDTO.getPermsName();
        String path = menuDTO.getPath();
        if (StringUtils.isEmpty(perms)||StringUtils.isEmpty(permsName)||StringUtils.isEmpty(path)) {
            throw new HerinRuntimeException("");
        }
        Menu menu = BeanUtil.copyProperties(menuDTO,Menu.class);
        LambdaQueryWrapper<Menu> menuWrapper = Wrappers.lambdaQuery();
        menuWrapper.eq(Menu::getPermsName,menu.getPermsName());
        Menu menu1 = menuMapper.selectOne(menuWrapper);
        if (menu1 != null) {
            throw new HerinRuntimeException("");
        } else {
            int flag = menuMapper.insert(menu);
            if (flag == 1) {
                redisCache.deleteObject("MenuList");
                return String.valueOf(ResultCode.SUCCESS);
            }else {
                throw new HerinRuntimeException("");
            }
        }

    }


}
