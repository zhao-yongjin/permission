package com.ext.service;

import com.ext.entity.KyDyss;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 地音监测系统实时数据 服务类
 * </p>
 *
 * @author 我的公众号：MarkerHub
 * @since 2022-12-07
 */
public interface KyDyssService extends IService<KyDyss> {

}
