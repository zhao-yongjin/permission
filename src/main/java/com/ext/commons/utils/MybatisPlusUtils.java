package com.ext.commons.utils;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.ArrayUtils;
import com.baomidou.mybatisplus.core.toolkit.LambdaUtils;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import com.baomidou.mybatisplus.core.toolkit.support.SerializedLambda;
import org.apache.ibatis.reflection.property.PropertyNamer;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

public class MybatisPlusUtils {
    @SafeVarargs
    public static <T> LambdaQueryWrapper<T> filterProperties(LambdaQueryWrapper<T> wrapper, SFunction<T, Serializable>... functions) {
        return Optional.ofNullable(functions).filter(ArrayUtils::isNotEmpty).map(array -> Arrays.stream(array).map(LambdaUtils::resolve).map(SerializedLambda::getImplMethodName).map(PropertyNamer::methodToProperty).collect(Collectors.toList())).map(properties -> wrapper.select(i -> !properties.contains(i.getProperty()))).orElse(wrapper);
    }}
