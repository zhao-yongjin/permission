package com.ext.commons.utils;



import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.format.annotation.DateTimeFormat;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.TemporalAdjusters;
import java.util.*;
import java.util.stream.Collectors;

/**
 * author: congbing
 * date: 2020-11-17 14:31
 * description: 日期工具类
 **/
public class DateUtils {

    private static final Logger logger = LogManager.getLogger(DateUtils.class);
    public static final DateTimeFormatter DATETIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    public static final DateTimeFormatter DATETIME_FORMATTER_HOUR = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:00:00");
    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    public static final DateTimeFormatter YEAR_FORMATTER = DateTimeFormatter.ofPattern("yyyy");
    public static final DateTimeFormatter MONTH_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM");

    /**
     * 将时间LocalDateTime参数格式化为yyyy-MM-dd HH:mm:ss字符串 默认样式
     *
     * @param localDateTime 时间参数 调用LocalDateTime.now()获取当前时间
     * @return 格式化为yyyy-MM-dd HH:mm:ss字符串
     */
    public static String format(LocalDateTime localDateTime) {
        return localDateTime.format(DATETIME_FORMATTER);
    }

    /**
     * 将时间LocalDateTime参数格式化为yyyy-MM-dd HH:mm:ss字符串 默认样式
     *
     * @param localDateTime 时间参数 调用LocalDateTime.now()获取当前时间
     * @return 格式化为yyyy-MM-dd HH:mm:ss字符串
     */
    public static LocalDateTime formatHour(LocalDateTime localDateTime) {
        return parseLocalDateTime(localDateTime.format(DATETIME_FORMATTER_HOUR));
    }

    /**
     * 将时间localDate参数格式化为yyyy-MM-dd HH:mm:ss字符串 默认样式
     *
     * @param localDate 时间参数 调用LocalDate.now()获取当前日期
     * @return 格式化为yyyy-MM-dd HH:mm:ss字符串
     */
    public static String format(LocalDate localDate) {
        return localDate.atTime(0, 0, 0).format(DATETIME_FORMATTER);
    }

    /**
     * 将时间LocalDateTime参数按传入pattern格式化为字符串
     *
     * @param localDateTime 时间参数 调用LocalDateTime.now()获取当前时间
     * @param pattern       格式样式
     * @return 格式化为pattern格式字符串
     */
    public static String formatDate(LocalDateTime localDateTime, String pattern) {
        return localDateTime.format(DateTimeFormatter.ofPattern(pattern));
    }

    /**
     * 将时间LocalDateTime参数格式化为yyyy-MM-dd字符串
     *
     * @param localDateTime 时间参数 调用LocalDateTime.now()获取当前时间
     * @return 格式化为yyyy-MM-dd字符串
     */
    public static String formatDate(LocalDateTime localDateTime) {
        return localDateTime.format(DATE_FORMATTER);
    }

    /**
     * 将时间LocalDate参数格式化为yyyy-MM-dd字符串
     *
     * @param localDateTime 时间参数 调用LocalDateTime.now()获取当前时间
     * @return 格式化为yyyy-MM-dd字符串
     */
    public static String formatDate(LocalDate localDateTime) {
        return localDateTime.format(DATE_FORMATTER);
    }

    /**
     * 将日期参数转化为字符串，默认格式为yyyy-MM-dd HH:mm:ss
     *
     * @param date 日期参数
     * @return 格式化为yyyy-MM-dd HH:mm:ss字符串
     */
    public static String format(Date date) {
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime().format(DATETIME_FORMATTER);
    }

    /**
     * 将日期参数转化为pattern样式的字符串
     *
     * @param date    日期参数
     * @param pattern 样式
     * @return 格式化为pattern样式的字符串
     */
    public static String format(Date date, String pattern) {
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime().format(DateTimeFormatter.ofPattern(pattern));
    }

    /**
     * 将日期参数转化为yyyy-MM-dd格式字符串
     *
     * @param date 日期参数
     * @return 格式化为yyyy-MM-dd字符串
     */
    public static String formatDate(Date date) {
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime().format(DATE_FORMATTER);
    }

    /**
     * 将yyyy-MM-dd HH:mm:ss样式日期字符串转化为Date对象
     *
     * @param dateStr 日期字符串参数
     * @return Date对象
     */
    public static Date parseFromDatetime(String dateStr) {
        return Date.from(LocalDateTime.parse(dateStr, DATETIME_FORMATTER).atZone(ZoneId.systemDefault()).toInstant());
    }

    /**
     * 将pattern样式日期字符串转化为Date对象
     *
     * @param dateStr 日期字符串参数
     * @return Date对象
     */
    public static Date parseFromDate(String dateStr) {
        return Date.from(LocalDate.parse(dateStr, DATE_FORMATTER).atTime(0, 0, 0).atZone(ZoneId.systemDefault()).toInstant());
    }

    /**
     * 将yyyy-MM-dd HH:mm:ss样式日期字符串转化为LocalDateTime对象
     *
     * @param dateStr 日期字符串参数
     * @return LocalDateTime对象
     */
    public static LocalDateTime parseLocalDateTime(String dateStr) {
        return LocalDateTime.parse(dateStr, DATETIME_FORMATTER);
    }

    /**
     * 将yyyy-MM-dd HH:mm:ss样式日期字符串转化为LocalDateTime对象
     *
     * @param dateStr 日期字符串参数
     * @return LocalDateTime对象
     */
    public static LocalDate parseLocalDate(String dateStr) {
        return LocalDate.parse(dateStr, DATE_FORMATTER);
    }

    /**
     * 将pattern样式日期字符串转化为LocalDateTime对象
     *
     * @param dateStr 日期字符串参数
     * @param pattern 样式
     * @return LocalDateTime对象
     */
    public static LocalDateTime parseLocalDateTime(String dateStr, String pattern) {
        return LocalDateTime.parse(dateStr, DateTimeFormatter.ofPattern(pattern));
    }

    /**
     * 将LocalDateTime转化为Date
     *
     * @param localDateTime LocalDateTime参数
     * @return Date对象
     */
    public static Date localDateTime2Date(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    /**
     * Date转LocalDateTime
     *
     * @param date date参数
     * @return LocalDateTime对象
     */
    public static LocalDateTime date2LocalDateTime(Date date) {
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    /**
     * 取本月第一天的LocalDate对象
     * 返回值调用getDayOfMonth()、getMonth()、getYear()等分别获取日、月、年，或者toString转化为字符串
     */
    public static LocalDate firstDayOfThisMonth() {
        return LocalDate.now().with(TemporalAdjusters.firstDayOfMonth());
    }

    /**
     * 获取本月第N天的LocalDate对象
     * 返回值调用getDayOfMonth()、getMonth()、getYear()等分别获取日、月、年，或者toString转化为字符串
     */
    public static LocalDate dayOfThisMonth(int n) {
        return LocalDate.now().withDayOfMonth(n);
    }

    /**
     * 取本月最后一天的LocalDate对象
     * 返回值调用getDayOfMonth()、getMonth()、getYear()等分别获取日、月、年，或者toString转化为字符串
     */
    public static LocalDate lastDayOfThisMonth() {
        return LocalDate.now().with(TemporalAdjusters.lastDayOfMonth());
    }

    /**
     * 取上月最后一天的LocalDate对象
     * 返回值调用getDayOfMonth()、getMonth()、getYear()等分别获取日、月、年，或者toString转化为字符串
     */
    public static LocalDate lastDayOfLastMonth() {
        return LocalDate.now().minusMonths(1L).with(TemporalAdjusters.lastDayOfMonth());
    }

    /**
     * 获取本月第一天时间 LocalDateTime对象
     */
    public static LocalDateTime startOfThisMonth() {
        return LocalDateTime.of(firstDayOfThisMonth(), LocalTime.of(0, 0, 0));
    }

    /**
     * 获取本月最后一天时间 LocalDateTime对象
     */
    public static LocalDateTime endOfThisMonth() {
        return LocalDateTime.of(lastDayOfThisMonth(), LocalTime.MAX);
    }

    /**
     * 获取上个月第一天
     */
    public static LocalDate firstDayOfLastMonth() {
        LocalDate localDate = LocalDate.now().minusMonths(1);
        return localDate.with(TemporalAdjusters.firstDayOfMonth());
    }

    /**
     * 获取上上个月第一天
     */
    public static LocalDate firstDayOfLastMonthBeforeLast() {
        //LocalDate localDate = LocalDate.parse("2020-01-01", DATE_FORMATTER).minusMonths(2);
        LocalDate localDate = LocalDate.now().minusMonths(2);
        return localDate.with(TemporalAdjusters.firstDayOfMonth());
    }

    /**
     * 获取N年前的第一天, 包含当年
     */
    public static LocalDate firstDayOfYearBefore(int n) {
        LocalDate localDate = LocalDate.now().minusYears(n - 1);
        return localDate.with((TemporalAdjusters.firstDayOfYear()));
    }

    /**
     * 获取N月前的第一天，不包含当月
     */
    public static LocalDate firstDayOfMonthBefore(int n) {
        LocalDate localDate = LocalDate.now().minusMonths(n);
        return localDate.with(TemporalAdjusters.firstDayOfMonth());
    }

    /**
     * 获取指定日期当前月的第一天
     * 参数为字符串
     */
    public static LocalDate getFirstDayOfMonthByDate(String localDateStr) {
        LocalDate date = LocalDate.parse(localDateStr, DATE_FORMATTER);
        return date.with(TemporalAdjusters.firstDayOfMonth());
    }

    /**
     * 获取指定日期当前月的第一天
     * 参数为LocalDate
     */
    public static LocalDate getFirstDayOfMonthByDate(LocalDate localDate) {
        return localDate.with(TemporalAdjusters.firstDayOfMonth());
    }

    /**
     * 获取指定日期区间月份, 包括右区间日期
     */
    public static List<LocalDate> getRangeMonthBetween(LocalDate startDate, LocalDate endDate) {
        List<LocalDate> range = new ArrayList<>();
        LocalDate temp;
        if (endDate.compareTo(startDate) >= 0) {
            long nums = endDate.toEpochDay() - startDate.toEpochDay();
            for (long i = 0; i <= nums; i++) {
                temp = startDate;
                if (temp.toEpochDay() - endDate.toEpochDay() <= 0) {
                    range.add(temp);
                    startDate = temp.plusMonths(1);
                }
            }
        }
        return range;
    }

    /**
     * 获取指定日期区间日期，包括右区间日期
     */
    public static List<LocalDate> getRangeDayBetween(LocalDate startDate, LocalDate endDate) {
        List<LocalDate> range = new ArrayList<>();
        LocalDate temp;
        if (endDate.compareTo(startDate) >= 0) {
            long nums = endDate.toEpochDay() - startDate.toEpochDay();
            for (long i = 0; i <= nums; i++) {
                temp = startDate;
                if (temp.toEpochDay() - endDate.toEpochDay() <= 0) {
                    range.add(temp);
                    startDate = temp.plusDays(1L);
                }
            }
        }
        return range;
    }

    /**
     * 获取指定日期区间年
     */
    public static List<LocalDate> getRangeYearBetween(LocalDate startDate, LocalDate endDate) {
        List<LocalDate> range = new ArrayList<>();
        LocalDate temp;
        if (endDate.compareTo(startDate) >= 0) {
            long nums = endDate.toEpochDay() - startDate.toEpochDay();
            for (long i = 0; i <= nums; i++) {
                temp = startDate;
                if (temp.toEpochDay() - endDate.toEpochDay() <= 0) {
                    range.add(temp);
                    startDate = temp.plusYears(1);
                }
            }
        }
        return range;
    }


    /**
     * 获取最近年份
     */
    public static List<String> getRecentYear(Integer yearCount) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
        Calendar cal = Calendar.getInstance();
        List<String> timeList = new ArrayList<>();
        for (int i = 0; i < yearCount; i++) {
            String dateString = sdf.format(cal.getTime());
            timeList.add(dateString);
            cal.add(Calendar.YEAR, -1);
        }
        return timeList;
    }

    /**
     * 获取最近月份
     */
    public static List<String> getRecentMonth(Integer monthCount) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
        Calendar cal = Calendar.getInstance();
//        Date date = new Date();

        List<String> timeList = new ArrayList<>();

        for (int i = 0; i < monthCount; i++) {
            String dateString = sdf.format(cal.getTime());
            timeList.add(dateString.substring(0, 7));
            cal.add(Calendar.MONTH, -1);
        }

        return timeList;
    }

//    /**
//     * 获取最近月份
//     * @return
//     */
//    public static List<String> getCurrentMonth(){
//        ArrayList<String> monthList = new ArrayList();
//        LocalDate today = LocalDate.now();
//        for(long i = 0L;i <= 11L; i++){
//            LocalDate localDate = today.minusMonths(i);
//            String month = localDate.toString().substring(0,7);
//            monthList.add(month);
//        }
//        return monthList;
//    }


    /**
     * 获取最近日期天数
     */
    public static List<String> getRecentDay(Integer dayCount) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        List<String> timeList = new ArrayList<>();
        for (int i = 0; i < dayCount; i++) {
            String dateString = sdf.format(cal.getTime());
            timeList.add(dateString);
            cal.add(Calendar.DATE, -1);
        }
        return timeList;
    }

    /**
     * 获取最近日期天数不包含当天
     */
    public static List<String> getDay(Integer dayCount) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        List<String> timeList = new ArrayList<>();
        for (int i = 0; i < dayCount; i++) {
            cal.add(Calendar.DATE, -1);
            String format = sdf.format(cal.getTime());
            timeList.add(format);
        }
        return timeList;
    }

    /**
     * 获取最近小时字符串
     */
    public static List<String> getRecentHour(Integer hourCount) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:00:00"); //时间格式化
        Calendar calendar = Calendar.getInstance();
        Date date = new Date();
        calendar.setTime(date);

        ArrayList<String> timeList = new ArrayList<>();

        for (int i = 0; i < hourCount; i++) {//当前时间循环24小时减下去
            calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY) - 1);
            String time = sdf.format(calendar.getTime());
//            System.out.println(time);
//            Timestamp ts = Timestamp.valueOf(time);
            timeList.add(time);
        }
        return timeList;
    }

    /**
     * 获取最近小时字符串
     */
    public static List<String> getHours(Integer hourCount) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:00:00"); //时间格式化
        Calendar calendar = Calendar.getInstance();
        Date date = new Date();
        calendar.setTime(date);

        ArrayList<String> timeList = new ArrayList<>();

        for (int i = 0; i < hourCount; i++) {//当前时间循环24小时减下去
            String time = sdf.format(calendar.getTime());
            timeList.add(time);
            calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY) - 1);
        }
        return timeList;
    }

    /**
     * 获取最近小时字符串
     */
    public static List<String> getRecentMinute() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:00"); //时间格式化
        SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //时间格式化
        Calendar calendar = Calendar.getInstance();
        Date date = new Date();
        calendar.setTime(date);

        ArrayList<String> timeList = new ArrayList<>();

        StringBuilder stringBuilder = new StringBuilder(sdf.format(calendar.getTime()));
        StringBuilder replace = stringBuilder.replace(15, 16, "0");
        calendar.set(Calendar.MINUTE, calendar.get(Calendar.MINUTE) - 10);
        String nowDate = String.valueOf(replace);
        Date parse = sd.parse(nowDate);
        calendar.setTime(parse);
        calendar.add(Calendar.DATE, -1);
        timeList.add(sd.format(calendar.getTime()));
        timeList.add(nowDate);
        return timeList;
    }

    /**
     * 获取固定月份
     */
    public static List<String> getStaticMonth() {
        ArrayList<String> monthList = new ArrayList<>();
        monthList.add("01");
        monthList.add("02");
        monthList.add("03");
        monthList.add("04");
        monthList.add("05");
        monthList.add("06");
        monthList.add("07");
        monthList.add("08");
        monthList.add("09");
        monthList.add("10");
        monthList.add("11");
        monthList.add("12");
        return monthList;
    }

    /**
     * 获取时间差 x小时x分钟
     *
     * @param startTime
     * @param endTime
     * @return
     */
    public static String getTimeBetween(String startTime, String endTime) {

        SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date fromDate;
            Date toDate;
            if (startTime == null) {
                fromDate = simpleFormat.parse(LocalDateTime.now().format(DATETIME_FORMATTER));
            } else {
                fromDate = simpleFormat.parse(startTime);
            }
            if (endTime == null) {
                toDate = simpleFormat.parse(LocalDateTime.now().format(DATETIME_FORMATTER));
            } else {
                toDate = simpleFormat.parse(endTime);
            }
            fromDate = simpleFormat.parse(startTime);
//            Date toDate = new Date();
            long from = fromDate.getTime();
            long to = toDate.getTime();
            long bettween = Math.abs(to - from);
            int hours = (int) (bettween / (1000 * 60 * 60));
            int minutes = (int) ((bettween / (1000 * 60)) - hours * 60);
            return hours + "小时" + minutes + "分钟";
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";

    }

    /**
     * 获取相差天数
     */
    public static int getTimeBetweenDay(String startTime, String endTime) {

        SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date fromDate;
            Date toDate;
            if (startTime == null) {
                fromDate = simpleFormat.parse(LocalDateTime.now().format(DATETIME_FORMATTER));
            } else {
                fromDate = simpleFormat.parse(startTime);
            }
            if (endTime == null) {
                toDate = simpleFormat.parse(LocalDateTime.now().format(DATETIME_FORMATTER));
            } else {
                toDate = simpleFormat.parse(endTime);
            }
            fromDate = simpleFormat.parse(startTime);
//            Date toDate = new Date();
            long from = fromDate.getTime();
            long to = toDate.getTime();
            long bettween = Math.abs(to - from);
            int day = (int) (bettween / (1000 * 60 * 60 * 24));


            return day;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;

    }

    /**
     * 获取时间差 x分
     *
     * @param startTime
     * @param endTime
     * @return
     */
    public static String getTimeBetweenMin(String startTime, String endTime) {

        SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date fromDate;
            Date toDate;
            if (startTime == null) {
                fromDate = simpleFormat.parse(LocalDateTime.now().format(DATETIME_FORMATTER));
            } else {
                fromDate = simpleFormat.parse(startTime);
            }
            if (endTime == null) {
                toDate = simpleFormat.parse(LocalDateTime.now().format(DATETIME_FORMATTER));
            } else {
                toDate = simpleFormat.parse(endTime);
            }
            fromDate = simpleFormat.parse(startTime);
            long from = fromDate.getTime();
            long to = toDate.getTime();
            long bettween = Math.abs(to - from);
            int minutes = (int) ((bettween / (1000 * 60)));
            return minutes + "分";
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";

    }

    /**
     * 是否超时
     */
    public static int getIsOutTime(String startTime, String endTime) {

        SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date fromDate;
            Date toDate;
            if (startTime == null) {
                fromDate = simpleFormat.parse(LocalDateTime.now().format(DATETIME_FORMATTER));
            } else {
                fromDate = simpleFormat.parse(startTime);
            }
            if (endTime == null) {
                toDate = simpleFormat.parse(LocalDateTime.now().format(DATETIME_FORMATTER));
            } else {
                toDate = simpleFormat.parse(endTime);
            }
            fromDate = simpleFormat.parse(startTime);
//            Date toDate = new Date();
            long from = fromDate.getTime();
            long to = toDate.getTime();
            long bettween = Math.abs(to - from);
            int hours = (int) (bettween / (1000 * 60 * 60));
            if (hours >= 8) {
                return 1;
            } else {
                return 0;
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;

    }

    /**
     * 生成近24小时的时间序列
     */
    public static String[] generate24Hour() {
        ArrayList<String> strings = new ArrayList<>();
        for (int i = 23; i >= 0; i--) {
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:00:00");
            //（1）获取当前时间
            LocalDateTime date = LocalDateTime.now();
            //（2）获取当前时间的前几小时时间
            LocalDateTime localDateTime = date.minusHours(i + 1);
            String time = dateTimeFormatter.format(localDateTime);
            strings.add(time);
        }
        return strings.toArray(new String[24]);
    }

    /**
     * 生成近48小时的时间序列
     */
    public static String[] generate48Hour() {
        ArrayList<String> strings = new ArrayList<>();
        for (int i = 47; i >= 0; i--) {
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:00:00");
            //（1）获取当前时间
            LocalDateTime date = LocalDateTime.now();
            //（2）获取当前时间的前几小时时间
            LocalDateTime localDateTime = date.minusHours(i + 1);
            String time = dateTimeFormatter.format(localDateTime);
            strings.add(time);
        }
        return strings.toArray(new String[48]);
    }

    /**
     * 生成48小时的时间序列
     */
    public static String[] generate48Hour(LocalDateTime endTime) {
        ArrayList<String> strings = new ArrayList<>();
        for (int i = 47; i >= 0; i--) {
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:00:00");
            //（1）获取当前时间
            LocalDateTime date = endTime;
            //（2）获取当前时间的前几小时时间
            LocalDateTime localDateTime = date.minusHours(i + 1);
            String time = dateTimeFormatter.format(localDateTime);
            strings.add(time);
        }
        return strings.toArray(new String[48]);
    }

    /**
     * 生成当天凌晨到最近1小时的时间序列
     */
    public static String[] generateNearlyDay() {
        ArrayList<String> strings = new ArrayList<>();
        int hour = LocalDateTime.now().getHour() - 1;
        for (int i = hour; i >= 0; i--) {
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:00:00");
            //（1）获取当前时间
            LocalDateTime date = LocalDateTime.now();
            //（2）获取当前时间的前几小时时间
            LocalDateTime localDateTime = date.minusHours(i + 1);
            String time = dateTimeFormatter.format(localDateTime);
            strings.add(time);
        }
        return strings.toArray(new String[hour]);
    }

    /**
     * 生成近N天的时间序列
     */
    public static String[] generateNDay(Integer day) {
        ArrayList<String> strings = new ArrayList<>();
        for (int i = (day - 1); i >= 0; i--) {
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd 00:00:00");
            //（1）获取当前时间
            LocalDateTime date = LocalDateTime.now();
            ZoneId zone = ZoneId.of("Asia/Shanghai");
            LocalDateTime dateTime = date.atZone(zone).toLocalDateTime();
            //（2）获取当前时间的前几小时时间
            LocalDateTime localDateTime = dateTime.minusDays(i + 1);
            String time = dateTimeFormatter.format(localDateTime);
            strings.add(time);
        }
        return strings.toArray(new String[day]);
    }


    /**
     * 生成n月的时间序列 包含当月
     */
    public static String[] generateNMonth(Integer month) {
        ArrayList<String> strings = new ArrayList<>();
        for (int i = month; i > 0; i--) {
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-01 00:00:00");
            //（1）获取当前时间
            LocalDateTime date = LocalDateTime.now();
            //（2）获取当前时间的前几小时时间
            LocalDateTime localDateTime = date.minusMonths(i);
            String time = dateTimeFormatter.format(localDateTime);
            strings.add(time);
        }
        return strings.toArray(new String[month]);
    }
    /**
     * 判断当前时间是否在时间段内
     * @param beginTimeStr 起始时间，格式应与format匹配
     * @param endTimeStr 结束时间，格式应与format匹配
     * @param format 时间格式，如HH:mm:ss
     * @return boolean
     */
    public static boolean belongCalendar(String beginTimeStr, String endTimeStr, String format) {

        SimpleDateFormat df = new SimpleDateFormat(format);
        Date nowTime = null;
        Date beginTime = null;
        Date endTime = null;
        try {
            nowTime = df.parse(df.format(new Date()));
            beginTime = df.parse(beginTimeStr);
            endTime = df.parse(endTimeStr);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Calendar date = Calendar.getInstance();
        date.setTime(nowTime);

        Calendar begin = Calendar.getInstance();
        begin.setTime(beginTime);

        Calendar end = Calendar.getInstance();
        end.setTime(endTime);

        if (date.after(begin) && date.before(end)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 当天00点时间戳
     *
     * @return long
     */
    public static long getZeroCurTimeMillis() {
        long current = System.currentTimeMillis();
        return  current / (1000 * 3600 * 24) * (1000 * 3600 * 24) - TimeZone.getDefault().getRawOffset();
    }






    public static void main(String[] args) {

    }
}
