package com.ext.commons.exception;

import javax.servlet.http.HttpServletResponse;

public class HerinRuntimeException extends RuntimeException{

    private Integer code;
    private String msg;
    private Object value;


    public HerinRuntimeException(Integer code) {
        this.code = code;
    }

    public HerinRuntimeException(String msg) {
        super(msg);
        this.code = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
        this.msg = msg;
    }

    public HerinRuntimeException(Integer code, String msg) {
        super(msg);
        this.msg = msg;
        this.code = code;
    }

    public HerinRuntimeException(Integer code, String msg, Object value) {
        super(msg);
        this.msg = msg;
        this.code = code;
        this.value = value;
    }

    public Integer getCode() {
        return code;
    }

    public com.ext.commons.exception.HerinRuntimeException setCode(Integer code) {
        this.code = code;
        return this;
    }

    public String getMsg() {
        return msg;
    }

    public com.ext.commons.exception.HerinRuntimeException setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public Object getValue() {
        return value;
    }
}