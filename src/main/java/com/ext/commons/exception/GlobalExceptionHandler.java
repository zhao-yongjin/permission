package com.ext.commons.exception;

import com.ext.commons.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    // 全局异常拦截
    @ExceptionHandler
    public R handlerHerinException(HerinRuntimeException e) {
        log.error("handlerHerinException", e);
        return R.fail(e.getCode(),e.getMsg());
    }

    // 处理运行异常
    @ExceptionHandler(RuntimeException.class)
    public R handleRuntimeException(RuntimeException exception) {
        log.error("handleRuntimeException: {}", exception);
        return R.fail(500, exception.getMessage());
    }

    // 捕获404，400这种无法到达controller的错误
    @ExceptionHandler(value = Exception.class)
    public R defaultErrorHandler(Exception exception) {
        int code;
        String msg;
        if (exception instanceof NoHandlerFoundException) {
            code = 404;
            msg = "未找到路径";
        } else if (exception instanceof BindException) {
            code = 412;
            msg = "接口参数校验失败";
        } else {
            code = 500;
            msg = "操作失败";
        }
        log.error("defaultErrorHandler: {}", exception);
        return R.fail(code, msg);
    }
    /**
     * 处理AccessDeineHandler无权限异常
     */
    @ExceptionHandler(value = AccessDeniedException.class)
    @ResponseBody
    public R exceptionHandler(HttpServletRequest req, AccessDeniedException e){
        throw e;
    }
}
