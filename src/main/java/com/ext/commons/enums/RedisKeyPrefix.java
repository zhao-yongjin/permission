package com.ext.commons.enums;

public class RedisKeyPrefix {
    /**
     * 井下人数
     */
    String PERSON_COUNT = "personCount:";

    /**
     * 井下领导
     */
    String PERSON_LEADER = "personLeader:";

}
