package com.ext.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@Data
@TableName("t_menu")
@ApiModel("菜单权限实体")
public class Menu {

    @TableId(type = IdType.AUTO)
    @ApiModelProperty("编码")
    private Integer id;
    @ApiModelProperty("菜单权限路径")
    private String perms;
    @ApiModelProperty("菜单权限名称")
    private String permsName;
    @ApiModelProperty("菜单url")
    private String path;
    @TableField(exist = false)
    private String pname;

//    //查询时不显示此字段
//    @ApiModelProperty("逻辑删除")
//    @TableField(select = false)
//    private  Integer deleted;
//
//    /**
//     * 菜单名
//     */
//    private String menuName;
//    /**
//     * 路由地址
//     */
//    private String path;
//    /**
//     * 组件路径
//     */
//    private String component;
//    /**
//     * 菜单状态（0显示 1隐藏）
//     */
//    private String visible;
//
//    /**
//     * 权限标识
//     */
//    private String perms;
//    /**
//     * 菜单图标
//     */
//    private String icon;
//
//    private Long createBy;
//
//    private Date createTime;
//
//    private Long updateBy;
//
//    private Date updateTime;
//    /**
//     * 是否删除（0未删除 1已删除）
//     */
//    private Integer delFlag;
//    /**
//     * 备注
//     */
//    private String remark;
//
//    // 字段添加填充内容
//    @TableField(fill = FieldFill.INSERT)
//    private Date gmtCreate;
//
//    @TableField(fill = FieldFill.INSERT_UPDATE)
//    private Date gmtModified;

}
