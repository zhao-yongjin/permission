package com.ext.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("t_dept")
@ApiModel("部门实体")
public class Dept  {

    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "编码")
    private Integer id;

    @ApiModelProperty(value = "部门名称",required = true)
    private String dName;

    @ApiModelProperty("部门描述")
    private String remark;

    @ApiModelProperty("创建人姓名")
    private String createName;

//    @ApiModelProperty("创建时间")
//    @TableField(fill = FieldFill.INSERT)
//    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//    private Date createTime;

    @ApiModelProperty("工资")
    private Integer salary;




//    //查询时不显示此字段
//    @ApiModelProperty("逻辑删除")
//    @TableField(select = false)
//    private  Integer deleted;


}
