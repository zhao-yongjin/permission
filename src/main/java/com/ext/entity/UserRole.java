package com.ext.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@TableName("t_user_role")
@ApiModel("用户角色关联实体")
@NoArgsConstructor
@AllArgsConstructor
public class UserRole {
    @ApiModelProperty("id")
    @TableId(type = IdType.AUTO)
    private Integer id;
    @ApiModelProperty("用户id")
    private Integer uid;
    @ApiModelProperty("角色id")
    private Integer rid;
    @ApiModelProperty("创建时间")
    @TableField(fill = FieldFill.INSERT) //创建时自动填充
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
//  @TableField(fill = FieldFill.INSERT_UPDATE)//创建与修改时自动填充
//  private Date updateTime;
}
