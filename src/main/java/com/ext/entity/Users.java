package com.ext.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ContentRowHeight(18)//内容行高
@HeadRowHeight(25)//标题行高
@ColumnWidth(20)//列宽，可设置成员变量上
public class Users implements Serializable {
    //index表示第几列（从0开始，表示第一列），value表示标题（表头）
    @ExcelProperty(value = "用户id",index = 0)
    private Integer id;

    @ExcelProperty(value = "用户名",index = 1)
    private String name;

    @ExcelProperty(value = "用户密码",index = 2)
    private String pwd;

    @ExcelProperty(value = "用户介绍",index = 3)
    private String userIntroduce;
}

