package com.ext.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@TableName("t_user")
@ApiModel("用户信息实体")
@NoArgsConstructor
@AllArgsConstructor
public class User {
    @ApiModelProperty("id")
    @TableId(type = IdType.AUTO)
    private Long  id;
    @ApiModelProperty("姓名")
    private String username;
    @ApiModelProperty("密码")
    private String password;
    @ApiModelProperty("性别")
    private Integer sex;
    @ApiModelProperty("邮箱")
    private String email;
    @ApiModelProperty("头像")
    private String imgPath;
    @ApiModelProperty("用户部门名称")
    private String userDeptName;
    @ApiModelProperty("用户部门名称")
    private String userRoleName;
    @ApiModelProperty("创建人姓名")
    private String createName;
    @ApiModelProperty("创建时间")
    @TableField(fill = FieldFill.INSERT) //创建时自动填充
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    @ApiModelProperty("部门id")
    private Integer deptId;
//
//    //查询时不显示此字段
//    @ApiModelProperty("逻辑删除")
//    @TableField(select = false)
//    private  Integer deleted;



}
