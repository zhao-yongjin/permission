package com.ext.entity;

import lombok.Data;

// 用户的token
@Data
public class UserToken {

    private Integer userId;

    private String username;
}
