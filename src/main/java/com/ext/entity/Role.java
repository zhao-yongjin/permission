package com.ext.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@TableName("t_role")
@ApiModel("角色实体")
@NoArgsConstructor
@AllArgsConstructor
public class Role {
    @ApiModelProperty("角色id")
    @TableId(type = IdType.AUTO)
    private Integer id;
    @ApiModelProperty("角色名称")
    private String rname;
    @ApiModelProperty("角色描述")
    private String rdesc;
    @ApiModelProperty("创建人姓名")
    private String createName;
    @ApiModelProperty("创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

//    //查询时不显示此字段
//    @ApiModelProperty("逻辑删除")
//    @TableField(select = false)
//    private  Integer deleted;
//    @TableField(exist = false)
//    private List<Integer> menuIds = new ArrayList<>();




}
