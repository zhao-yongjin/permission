package com.ext.entity;

import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author 我的公众号：MarkerHub
 * @since 2022-12-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TDeptHistory implements Serializable {

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ApiModelProperty("id")
    @TableId(value = "id")
    private Long  id;

    private String dname;

    private String remark;

    private String createName;

    private Date createTime;

    private Integer salary;


    public void setId(UUID randomUUID) {
    }
}
