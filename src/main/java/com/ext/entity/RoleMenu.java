package com.ext.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@TableName("t_role_menu")
@ApiModel("角色权限关联实体")
@NoArgsConstructor
@AllArgsConstructor
public class RoleMenu {
    @ApiModelProperty("id")
    @TableId(type = IdType.AUTO)
    private Integer id;
    @ApiModelProperty("用户id")
    private Integer rid;
    @ApiModelProperty("角色id")
    private Integer mid;
    @ApiModelProperty("创建时间")
    @TableField(fill = FieldFill.INSERT)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    //查询时不显示此字段
    @ApiModelProperty("逻辑删除")
    @TableField(select = false)
    private  Integer deleted;
}
