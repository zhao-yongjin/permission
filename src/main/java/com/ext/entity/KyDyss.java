package com.ext.entity;

import java.time.LocalDateTime;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 地音监测系统实时数据
 * </p>
 *
 * @author 我的公众号：MarkerHub
 * @since 2022-12-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("ky_dyss")
@AllArgsConstructor
@NoArgsConstructor
public class KyDyss implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 测点编号
     */
    private String eventCode;

    /**
     * 传感器位置
     */
    private String sensorLoc;

    /**
     * 记录地音事件的通道号
     */
    private String channelNum;

    /**
     * 有效事件能量
     */
    private String validEventEnergy;

    /**
     * 有效事件发生时间
     */
    private LocalDateTime validEventTime;


}
