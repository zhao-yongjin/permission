package com.ext.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ext.entity.Dept;
import com.ext.vo.DeptVO;
import com.ext.vo.PersonNumListVo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface DeptMapper extends BaseMapper<Dept> {


    List<DeptVO>  selectDept();


    List<PersonNumListVo> selectPersonCount();
}
