package com.ext.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.ext.entity.Menu;
import com.ext.vo.MenuVO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MenuMapper extends BaseMapper<Menu> {


    List<String> selectPermsByUserId(@Param("userId") Long userId);

    Integer selectPermsByRoleId(@Param("roleId") Integer roleId);

    List<MenuVO> selectMenu();
}
