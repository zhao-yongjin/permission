package com.ext.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ext.dto.RoleDTO;
import com.ext.entity.UserRole;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRoleMapper extends BaseMapper<UserRole> {


    Integer deleteByUserId(Integer id);

    Integer batchInsertUserRole(@Param("roleDTO")RoleDTO roleDTO);
}
