package com.ext.mapper;

import com.ext.entity.KyDyssHistory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 地音监测系统实时数据 Mapper 接口
 * </p>
 *
 * @author 我的公众号：MarkerHub
 * @since 2022-12-07
 */
public interface KyDyssHistoryMapper extends BaseMapper<KyDyssHistory> {

}
