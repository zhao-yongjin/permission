package com.ext.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ext.entity.Role;
import com.ext.vo.RoleVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleMapper extends BaseMapper<Role> {



    Integer getMenuCountById(@Param("roleId") Integer roleId);

    Integer selectByUserId(@Param("userId") Integer userId);

    //根据菜单id获取角色
    Integer selectRoleByMenuId(@Param("menuId") Integer menuId);

    List<RoleVO> selectRole();

}
