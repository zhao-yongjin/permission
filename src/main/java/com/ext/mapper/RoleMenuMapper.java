package com.ext.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ext.dto.GrantPermissionDTO;
import com.ext.entity.RoleMenu;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleMenuMapper extends BaseMapper<RoleMenu> {
    Integer deleteByMId(Integer id);

    Integer batchInsertRoleMenu(@Param("grantPermissionDTO") GrantPermissionDTO grantPermissionDTO);
}
