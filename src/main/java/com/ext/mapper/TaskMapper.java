package com.ext.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ext.entity.Task;

/**
 * <p>
 *  Mapper 接口
 * </p>
 */
public interface TaskMapper extends BaseMapper<Task> {

}
