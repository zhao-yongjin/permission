package com.ext.mapper;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ext.dto.RoleDTO;
import com.ext.entity.Menu;
import com.ext.entity.User;
import com.ext.vo.DeptVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;


import java.util.Map;

@Repository
public interface UserMapper extends BaseMapper<User> {

    Integer getUserCountByDeptId(@Param("deptId") Integer deptId);



}
