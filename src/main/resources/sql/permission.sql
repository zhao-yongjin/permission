/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 80029
 Source Host           : localhost:3306
 Source Schema         : permission

 Target Server Type    : MySQL
 Target Server Version : 80029
 File Encoding         : 65001

 Date: 17/11/2022 17:24:36
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_task
-- ----------------------------
DROP TABLE IF EXISTS `sys_task`;
CREATE TABLE `sys_task`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `job_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '任务名',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '任务描述',
  `cron_expression` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'cron表达式',
  `bean_class` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '任务执行时调用哪个类的方法 包名+类名',
  `job_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '任务状态',
  `job_group` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '任务分组',
  `create_user` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_user` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 32 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_task
-- ----------------------------

-- ----------------------------
-- Table structure for t_dept
-- ----------------------------
DROP TABLE IF EXISTS `t_dept`;
CREATE TABLE `t_dept`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `dname` varchar(14) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `remark` varchar(13) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_name` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `salary` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `dname`(`dname`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_dept
-- ----------------------------
INSERT INTO `t_dept` VALUES (1, '前端', '前端前端', 'tom', '2022-11-15 13:49:58', 3000);
INSERT INTO `t_dept` VALUES (2, 'java', '管理自己', 'tony', '2022-11-14 23:50:03', 3500);
INSERT INTO `t_dept` VALUES (3, 'efwfw', 'string', 'jack', '2022-11-14 19:50:07', 4000);
INSERT INTO `t_dept` VALUES (4, 'tests', '管理自己', 'string', '2022-11-14 19:50:12', 4500);
INSERT INTO `t_dept` VALUES (7, 'qwerty', '管理自己', '李四', '2022-11-14 18:50:17', 5000);
INSERT INTO `t_dept` VALUES (11, 'aaa', 'dfss', 'wdcas', '2022-11-14 17:50:21', 5500);
INSERT INTO `t_dept` VALUES (12, '张三', '三三', 'string', '2022-08-28 04:50:25', 6000);
INSERT INTO `t_dept` VALUES (14, 'saxsaxsxs', 'sfq', 'csacac', '2022-10-15 21:50:28', 6500);
INSERT INTO `t_dept` VALUES (22, 'aaaa', '', NULL, '2022-09-28 16:50:44', 7000);

-- ----------------------------
-- Table structure for t_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_menu`;
CREATE TABLE `t_menu`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `perms` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '权限表',
  `perms_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `path` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `url_name`(`perms_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_menu
-- ----------------------------
INSERT INTO `t_menu` VALUES (1, 'api:dept:list', '获取部门列表信息', 'dept/list');
INSERT INTO `t_menu` VALUES (2, 'api:dept:findData', '分页查询部门', 'dept/findData');
INSERT INTO `t_menu` VALUES (3, 'api:dept:save', '添加部门信息', 'dept/save');
INSERT INTO `t_menu` VALUES (4, 'api:dept:update', '修改部门信息', 'dept/update');
INSERT INTO `t_menu` VALUES (5, 'api:dept:remove', '根据id删除部门信息', 'dept/remove');
INSERT INTO `t_menu` VALUES (6, 'api:role:findData', '分页查询角色', 'role/list');
INSERT INTO `t_menu` VALUES (7, 'api:role:list', '获取角色信息列表', 'role/list');
INSERT INTO `t_menu` VALUES (8, 'api:role:save', '保存角色信息', 'role/update');
INSERT INTO `t_menu` VALUES (9, 'api:role:update', '修改角色信息', 'role/update');
INSERT INTO `t_menu` VALUES (10, 'api:role:remove', '根据id删除角色信息', 'role/remove');
INSERT INTO `t_menu` VALUES (11, 'api:user:list', '查询用户全部信息', 'user/list');
INSERT INTO `t_menu` VALUES (12, 'api:user:findData', '分页模糊查询用户信息', 'user/findData');
INSERT INTO `t_menu` VALUES (13, 'api:user:save', '保存用户信息', 'user/save');
INSERT INTO `t_menu` VALUES (14, 'api:user:update', '修改用户的信息', 'user/update');
INSERT INTO `t_menu` VALUES (15, 'api:user:remove', '根据id删除用户信息', 'user/remove');
INSERT INTO `t_menu` VALUES (16, 'api:dept:deptCache', '获取全部用户带缓存', 'dept/deptCache');
INSERT INTO `t_menu` VALUES (17, 'api:menu:menuCache', '菜单信息带缓存', 'menu/menuCache');
INSERT INTO `t_menu` VALUES (18, 'api:menu:list', '获取菜单权限信息列表', 'menu/list');
INSERT INTO `t_menu` VALUES (19, 'api:menu:findData', '权限分页查询', 'menu/findData');
INSERT INTO `t_menu` VALUES (20, 'api:menu:save', '保存权限', 'menu/save');
INSERT INTO `t_menu` VALUES (21, 'api:menu:update', '修改权限', 'menu/update');
INSERT INTO `t_menu` VALUES (22, 'api:menu:remove', '根据菜单id删除菜单权限信息', 'menu/remove');
INSERT INTO `t_menu` VALUES (23, 'api:user:uppwd', '修改密码', 'user/uppwd');
INSERT INTO `t_menu` VALUES (24, 'string', 'string', 'string');

-- ----------------------------
-- Table structure for t_role
-- ----------------------------
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `rname` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `rdesc` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `create_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `rname`(`rname`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_role
-- ----------------------------
INSERT INTO `t_role` VALUES (1, 'admin', '管理系统', '2022-07-06 09:33:29', '张三');
INSERT INTO `t_role` VALUES (2, 'hfergerggs', 'grgeger', '2022-08-16 16:23:09', '李四');
INSERT INTO `t_role` VALUES (3, 'test', '修改1', '2022-08-24 17:14:50', 'string');
INSERT INTO `t_role` VALUES (4, 'wyjcvywu', 'fewfwef', '2022-08-01 17:28:53', 'vdvs');
INSERT INTO `t_role` VALUES (5, 'sdvn', 'csax', '2022-08-16 17:28:56', 'geww');
INSERT INTO `t_role` VALUES (6, 'sdvnnoni', 'sacds', '2022-08-30 15:36:38', 'gnononi');

-- ----------------------------
-- Table structure for t_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_role_menu`;
CREATE TABLE `t_role_menu`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `rid` int NULL DEFAULT NULL,
  `mid` int NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 48 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_role_menu
-- ----------------------------
INSERT INTO `t_role_menu` VALUES (1, 1, 1, '2022-07-22 16:24:48');
INSERT INTO `t_role_menu` VALUES (2, 1, 2, '2022-07-22 16:24:48');
INSERT INTO `t_role_menu` VALUES (3, 1, 3, '2022-07-22 16:24:48');
INSERT INTO `t_role_menu` VALUES (4, 1, 4, '2022-07-22 16:24:48');
INSERT INTO `t_role_menu` VALUES (5, 1, 5, '2022-07-22 16:24:48');
INSERT INTO `t_role_menu` VALUES (6, 1, 6, '2022-07-22 16:24:48');
INSERT INTO `t_role_menu` VALUES (7, 1, 7, '2022-07-22 16:24:48');
INSERT INTO `t_role_menu` VALUES (8, 1, 8, '2022-07-22 16:24:48');
INSERT INTO `t_role_menu` VALUES (9, 1, 9, '2022-07-22 16:24:48');
INSERT INTO `t_role_menu` VALUES (10, 1, 10, '2022-07-22 16:24:48');
INSERT INTO `t_role_menu` VALUES (11, 1, 11, '2022-07-22 16:40:03');
INSERT INTO `t_role_menu` VALUES (12, 1, 12, '2022-07-22 16:40:03');
INSERT INTO `t_role_menu` VALUES (13, 1, 13, '2022-07-22 16:40:03');
INSERT INTO `t_role_menu` VALUES (14, 1, 14, '2022-07-22 16:40:03');
INSERT INTO `t_role_menu` VALUES (17, 2, 11, '2022-08-18 09:33:17');
INSERT INTO `t_role_menu` VALUES (18, 2, 13, '2022-08-12 09:36:29');
INSERT INTO `t_role_menu` VALUES (19, 2, 14, '2022-08-16 09:36:33');
INSERT INTO `t_role_menu` VALUES (20, 1, 15, '2022-08-11 14:19:17');
INSERT INTO `t_role_menu` VALUES (21, 1, 16, '2022-08-01 14:19:33');
INSERT INTO `t_role_menu` VALUES (22, 1, 17, '2022-08-15 09:51:35');
INSERT INTO `t_role_menu` VALUES (23, 1, 18, '2022-08-02 10:08:43');
INSERT INTO `t_role_menu` VALUES (24, 1, 19, '2022-08-02 10:09:07');
INSERT INTO `t_role_menu` VALUES (25, 1, 20, '2022-08-04 10:09:16');
INSERT INTO `t_role_menu` VALUES (26, 1, 21, '2022-08-03 10:09:34');
INSERT INTO `t_role_menu` VALUES (27, 1, 22, '2022-08-15 10:09:50');
INSERT INTO `t_role_menu` VALUES (28, 1, 23, '2022-08-22 10:42:37');
INSERT INTO `t_role_menu` VALUES (29, 2, 24, '2022-08-02 17:48:03');
INSERT INTO `t_role_menu` VALUES (33, 4, 8, '2022-08-29 11:37:11');
INSERT INTO `t_role_menu` VALUES (34, 4, 9, '2022-08-29 11:37:11');
INSERT INTO `t_role_menu` VALUES (35, 5, 1, '2022-09-01 10:17:44');
INSERT INTO `t_role_menu` VALUES (41, 5, 7, '2022-09-02 10:29:31');
INSERT INTO `t_role_menu` VALUES (42, 3, 11, '2022-09-05 16:24:57');
INSERT INTO `t_role_menu` VALUES (43, 3, 12, '2022-09-05 16:24:57');
INSERT INTO `t_role_menu` VALUES (44, 3, 13, '2022-09-05 16:24:57');
INSERT INTO `t_role_menu` VALUES (45, 3, 14, '2022-09-05 16:24:57');
INSERT INTO `t_role_menu` VALUES (46, 3, 15, '2022-09-05 16:24:57');
INSERT INTO `t_role_menu` VALUES (47, 3, 23, '2022-09-05 16:24:57');

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `sex` int NULL DEFAULT NULL,
  `email` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `dept_id` int NULL DEFAULT NULL,
  `img_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `user_dept_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `user_role_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `create_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 61 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES (1, '张三', '$2a$10$amLfJR.Uu1lGgsl72TXN6OtL.JeVI52R0vPuld9h1OvmPkJukjt6i', 1, 'zhangsan@qq.com', '2022-08-09 10:09:51', 1, NULL, '测试', '普通用户', '李四');
INSERT INTO `t_user` VALUES (2, 'admin', '$2a$10$LSHikHa55RKvGNa3Hhno9e.ZBYGTmkYxTEsUXsPSvJFJ7b8cQMF1i', 1, 'admin@qq.com', '2022-08-16 10:09:54', 2, '', 'java', 'admin', '张三');
INSERT INTO `t_user` VALUES (3, 'toString', '$2a$10$amLfJR.Uu1lGgsl72TXN6OtL.JeVI52R0vPuld9h1OvmPkJukjt6i', 1, 'admin@qf.com', '2022-08-15 10:10:01', 3, NULL, '前端', '普通用户', 'tony');
INSERT INTO `t_user` VALUES (4, 'test', '$2a$10$joroh2Zs/y4d/6SV6posnOyx4347hjFhKokyZzRNIvkyrs0w8pOuG', 1, 'admin@qf.comadsd', '2022-08-29 10:10:05', 4, '', '测试', '普通用户', '张三');
INSERT INTO `t_user` VALUES (55, NULL, NULL, NULL, NULL, '2022-09-02 15:28:44', NULL, '172.16.0.51/zyj/c3cdae5f-bce6-44dd-9401-bae1f1aab247.jpg', NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (58, NULL, NULL, NULL, NULL, '2022-09-03 08:52:19', NULL, 'D:\\file\\imge6acf229-1b0c-47f4-9f70-79ac72c0c1ac.jpg', NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (59, NULL, NULL, NULL, NULL, '2022-09-06 14:16:09', NULL, '172.16.0.51/zyj/0a24e46a-bd2c-4de4-b9ca-0cffb94a997a.jpg', NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (60, NULL, NULL, NULL, NULL, '2022-09-06 14:24:34', NULL, '172.16.0.51/zyj/f65b109b-0821-422c-9584-6fd5ec1f57e5.jpg', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for t_user_role
-- ----------------------------
DROP TABLE IF EXISTS `t_user_role`;
CREATE TABLE `t_user_role`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `uid` int NULL DEFAULT NULL,
  `rid` int NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_user_role
-- ----------------------------
INSERT INTO `t_user_role` VALUES (1, 1, 1, '2022-07-27 17:44:30');
INSERT INTO `t_user_role` VALUES (2, 2, 1, '2022-08-03 17:44:55');
INSERT INTO `t_user_role` VALUES (5, 4, 2, '2022-08-03 17:44:45');
INSERT INTO `t_user_role` VALUES (20, 4, 3, '2022-09-02 10:55:16');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int NOT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `pwd` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `userIntroduce` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------

-- ----------------------------
-- Table structure for video_upload
-- ----------------------------
DROP TABLE IF EXISTS `video_upload`;
CREATE TABLE `video_upload`  (
  `id` int NOT NULL,
  `video_name` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `video_url` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `videoUUID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of video_upload
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
